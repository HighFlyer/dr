package ru.highflyer.dr.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 10/26/13 8:25 PM
 */
final public class EaseView extends View {
    private final Paint paint = new Paint();

    public EaseView( Context context, AttributeSet attrs ) {
        super(context, attrs);

        paint.setDither(true);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @Override
    protected void onDraw( Canvas canvas ) {
        super.onDraw(canvas);

        canvas.drawCircle(getWidth() / 2, getHeight() / 2, Math.min(getWidth(), getHeight()) / 2, paint);
    }

    public void setEase( float ease ) {
        int color = Color.rgb((int)((1 - ease) * 255), (int)(255 * ease), 0);
        paint.setColor(color);
        invalidate();
    }
}
