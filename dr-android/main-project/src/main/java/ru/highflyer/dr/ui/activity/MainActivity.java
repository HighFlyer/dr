package ru.highflyer.dr.ui.activity;

import android.app.PendingIntent;
import android.content.*;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import org.json.JSONException;
import org.json.JSONObject;
import ru.highflyer.dr.R;
import ru.highflyer.dr.app.TimerReceiver;
import ru.highflyer.dr.db.DRContentProvider;
import ru.highflyer.dr.log.LogUtils;
import ru.highflyer.dr.timer.TimerManager;
import ru.highflyer.dr.ui.fragment.ChooseQuestionFragment;
import ru.highflyer.dr.ui.fragment.QuestionFragment;
import ru.highflyer.dr.ui.fragment.RateDialogFragment;
import ru.highflyer.dr.ui.fragment.RepeatFragment;
import ru.highflyer.dr.ui.preferences.TicketOrderUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13 2:45 PM
 */
final public class MainActivity extends ActionBarActivity
        implements LoaderManager.LoaderCallbacks<Cursor>,
        QuestionFragment.QuestionFragmentListener,
        RepeatFragment.RepeatFragmentListener, ChooseQuestionFragment.ChooseQuestionFragmentListener {
    final static private int LOADER_QUESTION_IDS = 0;

    final static private int REQUEST_CODE_PURCHASE_ADS_FREE = 0;
    final static private int REQUEST_CHOOSE_TICKET = 1;
    final static private int REQUEST_CHOOSE_QUESTION = 2;

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String EXTRA_TICKET_NUMBER = "TICKET_NUMBER";
    private static final String PURCHASE_ADS_FREE = "ru.highflyer.dr.adsfree";
//    private static final String PURCHASE_ADS_FREE = "android.test.refunded";
    private static final String INAPP_TYPE = "inapp";

    final private DecimalFormat decimalFormat = new DecimalFormat("#.#");

    private View progress;
    private AdView adView;
    private long questionStartTime;
    private boolean isBillingSupported;

    private String ticketNumber;

    private IInAppBillingService purchaseService;
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected( ComponentName name, IBinder service ) {
            purchaseService = IInAppBillingService.Stub.asInterface(service);

            updateIsBillingSupported();
            fetchListOfPurchasedItems();
            supportInvalidateOptionsMenu();
        }

        @Override
        public void onServiceDisconnected( ComponentName name ) {
            purchaseService = null;
            supportInvalidateOptionsMenu();
        }
    };
    private BroadcastReceiver notificationReceiver;

    private void updateIsBillingSupported( ) {
        try {
            isBillingSupported = purchaseService.isBillingSupported(3, getPackageName(), INAPP_TYPE) == 0;
        } catch (RemoteException e) {
            Log.e(TAG, "Method call failed", e);
        }
    }

    private void fetchListOfPurchasedItems( ) {
        try {
            String token = null;
            do {
                Bundle ownedItems = purchaseService.getPurchases(3, getPackageName(), INAPP_TYPE, token);
                int response = ownedItems.getInt("RESPONSE_CODE");
                Log.d(TAG, String.format("Owned items fetched, RESPONSE_CODE: %d", response));
                if (response == 0) {
                    // if continuationToken != null, call getPurchases again
                    // and pass in the token to retrieve more items
                    token = ownedItems.getString("INAPP_CONTINUATION_TOKEN");
                    ArrayList<String> ownedSkus =
                            ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");

                    Log.d(TAG, String.format("Owned SKUs: %s", ownedSkus));

                    for (int i = 0; i < ownedSkus.size(); ++i) {
                        String sku = ownedSkus.get(i);

                        // do something with this purchase information
                        // e.g. display the updated list of products owned by user
                        if (PURCHASE_ADS_FREE.equals(sku)) {
                            onAdsFreePurchased();
                            /* We want to finish */
                            token = null;
                            break;
                        }
                    }
                }
                else
                    token = null;
            } while (!TextUtils.isEmpty(token));
        } catch (RemoteException e) {
            Log.e(TAG, "Failed to call method", e);
        }
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        requestWindowFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.main_activity);
        setSupportProgressBarVisibility(true);

        progress = findViewById(R.id.progress);

        if (!TicketOrderUtils.isAdsFree(this)) {
            initAdmob();

            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }

        if (savedInstanceState == null)
            RateDialogFragment.showDialogIfTime(this);
        else
            ticketNumber = savedInstanceState.getString(EXTRA_TICKET_NUMBER);

        if (getSupportFragmentManager().findFragmentById(R.id.question) == null)
            startQuestionProcessing();
        else
            progress.setVisibility(View.GONE);
    }

    private void initAdmob( ) {
        adView = (AdView)findViewById(R.id.ad_view);
        if (adView == null)
            return;

        AdRequest adRequest = new AdRequest.Builder().
                addTestDevice(AdRequest.DEVICE_ID_EMULATOR).
                build();
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed( ) {
            }

            @Override
            public void onAdFailedToLoad( int errorCode ) {
                adView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLeftApplication( ) {
            }

            @Override
            public void onAdOpened( ) {
            }

            public void onAdLoaded( ) {
                adView.setVisibility(View.VISIBLE);
            }
        });
        adView.loadAd(adRequest);
    }

    @Override
    protected void onPause( ) {
        super.onPause();

        pauseAdvertisment();

        if (notificationReceiver != null) {
            unregisterReceiver(notificationReceiver);
            notificationReceiver = null;
        }
    }

    private void clearAdvertisment( ) {
        pauseAdvertisment();
        if (adView != null) {
            adView.destroy();
            adView.setVisibility(View.GONE);
            adView = null;
        }
    }

    private void pauseAdvertisment( ) {
        if (adView != null)
            adView.pause();
    }

    @Override
    protected void onResume( ) {
        super.onResume();
        if (adView != null)
            adView.resume();

        if (notificationReceiver == null) {
            notificationReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive( Context context, Intent intent ) {
                    abortBroadcast();
                }
            };

            IntentFilter filter = new IntentFilter(TimerReceiver.ACTION_TIMER);
            filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
            registerReceiver(notificationReceiver, filter);
        }

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.cancel(TimerReceiver.NOTIFICATION_ID);
    }

    @Override
    protected void onStart( ) {
        super.onStart();

        LogUtils.startSession(this);
        questionStartTime = System.currentTimeMillis();
    }

    @Override
    protected void onStop( ) {
        super.onStop();

        LogUtils.endSession(this);
        long answeringTime = System.currentTimeMillis() - questionStartTime;
        TicketOrderUtils.increaseTotalDurationBy(this, answeringTime);
    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        super.onSaveInstanceState(outState);

        outState.putString(EXTRA_TICKET_NUMBER, ticketNumber);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.ads_free).setVisible(isBillingSupported && purchaseService != null &&
                !TicketOrderUtils.isAdsFree(this));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
        case R.id.choose_question:
            ChooseQuestionFragment chooseFragment = ChooseQuestionFragment.newInstance(R.string.dialog_choose_ticket, 40, REQUEST_CHOOSE_TICKET);
            chooseFragment.show(getSupportFragmentManager(), "choose-ticket");
            LogUtils.logChooseQuestion();
            return true;
        case R.id.ads_free:
            try {
                Bundle buyIntentBundle = purchaseService.getBuyIntent(3, getPackageName(),
                        PURCHASE_ADS_FREE, INAPP_TYPE, null);
                PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

                startIntentSenderForResult(pendingIntent.getIntentSender(), REQUEST_CODE_PURCHASE_ADS_FREE, new Intent(), 0, 0, 0);
            } catch (Exception e) {
                Toast.makeText(this, R.string.start_buy_ads_free_failed, Toast.LENGTH_LONG).show();
                Log.e(TAG, "Failed to buy adsfree", e);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader( int id, Bundle bundle ) {
        switch (id) {
        case LOADER_QUESTION_IDS:
            return new CursorLoader(this, DRContentProvider.questionsUri(), new String [] {"_id"}, null, null, "RANDOM()");
        default:
            throw new IllegalArgumentException("Don't know " + id + " id");
        }
    }

    @Override
    public void onLoadFinished( Loader loader, Cursor c ) {
        switch (loader.getId()) {
        case LOADER_QUESTION_IDS:
            questionIdsLoaded(c);
            break;
        }
    }

    private void questionIdsLoaded( Cursor c ) {
        if (!TicketOrderUtils.isOrderPresents(this))
            TicketOrderUtils.fillByCursor(this, c);

        questionStartTime = System.currentTimeMillis();
        showNextQuestion();
    }

    @Override
    public void onLoaderReset( Loader loader ) {
    }

    @Override
    public void onShowNextQuestion(String questionId) {
        TicketOrderUtils.removeFirstTicket(this, questionId);
        showNextQuestion();
    }

    @Override
    public void onQuestionAnsweredRight( String questionId ) {
        TicketOrderUtils.removeFirstTicket(this, questionId);
        showNextQuestion();
    }

    @Override
    public void onImageClicked( View view, Bitmap image ) {
        Intent intent = new Intent(this, PhotoViewActivity.class);
        intent.putExtra(PhotoViewActivity.KEY_BITMAP, image);
        startActivity(intent);
    }

    private void showNextQuestion( ) {
        if (isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && isDestroyed()))
            return;

        progress.setVisibility(View.GONE);

        final List<String> ids = TicketOrderUtils.getOrderIds(this);
        int originalCount = TicketOrderUtils.getTotalQuestionsCount(this);
        float progressPercent = originalCount > 0 ? 100.0f *
                (originalCount - TicketOrderUtils.getUniqueIdsCount(this)) / originalCount : 0;
        String status = getString(R.string.progress,
                decimalFormat.format(progressPercent),
                ids.size());
        getSupportActionBar().setSubtitle(status);
        setSupportProgress((int)(progressPercent * 100));
        if (ids.isEmpty()) {
            progress.post(new Runnable() {
                @Override
                public void run() {
                    showRepeatDialog();
                }
            });

            return;
        }

        LogUtils.pageView();
        getSupportFragmentManager().beginTransaction().
                replace(R.id.question, QuestionFragment.newInstance(ids.get(0))).commitAllowingStateLoss();
    }

    private void showRepeatDialog( ) {
        TicketOrderUtils.setPassedOnce(this);
        LogUtils.logRepeatDialog();
        TicketOrderUtils.increaseTotalDurationBy(this, System.currentTimeMillis() - questionStartTime);

        RepeatFragment.newInstance().show(getSupportFragmentManager(), "repeat-dialog");

        long totalDuration = TicketOrderUtils.getTotalDuration(this);
        if (TicketOrderUtils.getStartTime(this) > 0 && totalDuration > 0)
            LogUtils.logTotalDuration(totalDuration);
    }

    @Override
    public void onRepeatFragmentClosed( ) {
        TimerManager.cancelPrompt(this);
        TicketOrderUtils.removeAllSettings(this);
        getSupportLoaderManager().restartLoader(LOADER_QUESTION_IDS, null, this);
    }

    private void startQuestionProcessing() {
        if (TicketOrderUtils.isOrderPresents(this))
            showNextQuestion();
        else
            getSupportLoaderManager().restartLoader(LOADER_QUESTION_IDS, null, this);
    }

    @Override
    protected void onActivityResult( int request, int response, Intent data ) {
        switch (request) {
        case REQUEST_CODE_PURCHASE_ADS_FREE:
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (response == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    if (TextUtils.equals(PURCHASE_ADS_FREE, sku)) {
                        onAdsFreePurchased();
                        Toast.makeText(this, R.string.buy_ads_free_success, Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    Log.e(TAG, "Failed to parse response", e);
                    Toast.makeText(this, R.string.buy_ads_free_failed, Toast.LENGTH_LONG).show();
                }
            }
            break;
        default:
            super.onActivityResult(request, response, data);
        }
    }

    private void onAdsFreePurchased( ) {
        TicketOrderUtils.setAdsFree(this, true);
        clearAdvertisment();

        if (purchaseService != null)
            unbindService(serviceConnection);
    }

    @Override
    protected void onDestroy( ) {
        super.onDestroy();
        if (adView != null)
            adView.destroy();

        if (purchaseService != null)
            unbindService(serviceConnection);
    }

    @Override
    public void onNumberSelected( String number, int requestCode ) {
        switch (requestCode) {
        case REQUEST_CHOOSE_TICKET:
            ticketNumber = number;

            ChooseQuestionFragment fragment = ChooseQuestionFragment.
                    newInstance(R.string.dialog_choose_question, 20, REQUEST_CHOOSE_QUESTION);
            fragment.show(getSupportFragmentManager(), "choose-question");

            break;
        case REQUEST_CHOOSE_QUESTION:
            LogUtils.pageView();

            LogUtils.logQuestionChosen(ticketNumber, number);
            new FindQuestionIdAsyncTask(this).execute(new Pair<>(ticketNumber, number));
        }
    }
}