package ru.highflyer.dr.ui.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/9/13 3:12 PM
 */
final public class TicketOrderUtils {
    final static private String PREFERENCES_NAME = TicketOrderUtils.class.getSimpleName();

    final static private String KEY_ORDER = "ORDER";
    final static private String KEY_ORIGINAL_QUESTIONS_COUNT = "ORIGINAL_QUESTIONS_COUNT";
    final static private String KEY_INCORRECT_ANSWERED_COUNT = "INCORRECT_ANSWERED_COUNT";
    final static private String KEY_START_TIME = "START_TIME";
    final static private String KEY_TOTAL_DURATION = "TOTAL_DURATION";
    final static private String KEY_ADS_FREE = "ADS_FREE";
    final static private String KEY_PASSED_AT_LEAST_ONE_TIME = "PASSED_AT_LEAST_ONE_TIME";
    final static private String KEY_NOTIFICATIONS_CANCELLED = "KEY_NOTIFICATIONS_CANCELLED";
    final static private String DELIMITER = ",";

    static private List<String> CACHED_IDS;
    static private Executor executor = Executors.newSingleThreadExecutor();

    static public boolean isOrderPresents( Context context ) {
        SharedPreferences preferences = openPreferences(context);
        return preferences.contains(KEY_ORDER);
    }

    public static void fillByCursor( Context context, Cursor c ) {
        List<String> ids = new ArrayList<>();
        while (c.moveToNext())
            ids.add(c.getString(c.getColumnIndex("_id")));

        SharedPreferences.Editor editor = openEditor(context).putInt(KEY_ORIGINAL_QUESTIONS_COUNT, ids.size());
        editor.putLong(KEY_START_TIME, System.currentTimeMillis());
        flushIds(ids, editor);
    }

    private static void flushIds( List<String> ids, SharedPreferences.Editor editor ) {
        CACHED_IDS = new ArrayList<>(ids);

        editor.putString(KEY_ORDER, TextUtils.join(DELIMITER, ids));

        commitEditor(editor);
    }

    private static void commitEditor( final SharedPreferences.Editor editor ) {
        executor.execute(new Runnable() {
            @Override
            public void run( ) {
                editor.commit();
            }
        });
    }

    private static SharedPreferences openPreferences( Context context ) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor openEditor( Context context ) {
        return openPreferences(context).edit();
    }

    public static @NonNull List<String> getOrderIds( Context context ) {
        if (CACHED_IDS == null) {
            String savedString = openPreferences(context).getString(KEY_ORDER, "");
            if (TextUtils.isEmpty(savedString))
                CACHED_IDS = new ArrayList<>();
            else
                CACHED_IDS = new ArrayList<>(Arrays.asList(savedString.split(DELIMITER)));
        }

        return CACHED_IDS;
    }

    public static void removeFirstTicket( Context context, String questionId ) {
        List<String> ids = getOrderIds(context);
        if (TextUtils.equals(ids.get(0), questionId)) {
            ids.remove(0);
            flushIds(ids, openEditor(context));
        }
    }

    public static void removeFirstTicketAndShuffle( Context context, String questionId ) {
        List<String> ids = getOrderIds(context);
        String id = ids.get(0);
        if (!TextUtils.equals(questionId, id))
            return;

        removeAllEntries(ids, id);

        ids.add(Math.min(15, Math.max(0, ids.size() - 1)), id);
        if (ids.size() > 40)
            ids.add(40, id);

        flushIds(ids, openEditor(context));
    }

    private static void removeAllEntries( List<String> ids, String id ) {
        for (Iterator<String> it = ids.iterator(); it.hasNext(); )
            if (it.next().equals(id))
                it.remove();
    }

    public static int getIncorrectNumber( Context context ) {
        return openPreferences(context).getInt(KEY_INCORRECT_ANSWERED_COUNT, 0);
    }

    public static void increaseIncorrectNumber( Context context ) {
        commitEditor(openEditor(context).putInt(KEY_INCORRECT_ANSWERED_COUNT, getIncorrectNumber(context) + 1));
    }

    public static int getTotalQuestionsCount( Context context ) {
        return openPreferences(context).getInt(KEY_ORIGINAL_QUESTIONS_COUNT, 0);
    }

    public static int getUniqueIdsCount( Context context ) {
        return new HashSet<>(getOrderIds(context)).size();
    }

    public static void removeAllSettings( Context context ) {
        SharedPreferences.Editor editor = openEditor(context);
        editor.remove(KEY_ORDER);
        editor.remove(KEY_ORIGINAL_QUESTIONS_COUNT);
        editor.remove(KEY_INCORRECT_ANSWERED_COUNT);
        editor.remove(KEY_TOTAL_DURATION);
        editor.remove(KEY_START_TIME);

        commitEditor(editor);
    }

    public static long getStartTime( Context context ) {
        return openPreferences(context).getLong(KEY_START_TIME, 0);
    }

    public static long getTotalDuration( Context context ) {
        return openPreferences(context).getLong(KEY_TOTAL_DURATION, 0);
    }

    public static void increaseTotalDurationBy( Context context, long deltaTime ) {
        deltaTime = Math.abs(deltaTime);
        commitEditor(openEditor(context).putLong(KEY_TOTAL_DURATION, getTotalDuration(context) + deltaTime));
    }

    public static boolean isAdsFree( Context context ) {
        return openPreferences(context).getBoolean(KEY_ADS_FREE, false);
    }

    public static void setAdsFree( Context context, boolean isAdsFree ) {
        commitEditor(openEditor(context).putBoolean(KEY_ADS_FREE, isAdsFree));
    }

    public static void setPassedOnce( Context context ) {
        commitEditor(openEditor(context).putBoolean(KEY_PASSED_AT_LEAST_ONE_TIME, true));
    }

    public static boolean isPassedOnce( Context context ) {
        return openPreferences(context).getBoolean(KEY_PASSED_AT_LEAST_ONE_TIME, false);
    }

    public static boolean isNotificationsCancelled( Context context ) {
        return openPreferences(context).getBoolean(KEY_NOTIFICATIONS_CANCELLED, false);
    }

    public static void setNotificationsCancelled( Context context ) {
        commitEditor(openEditor(context).putBoolean(KEY_NOTIFICATIONS_CANCELLED, true));
    }
}
