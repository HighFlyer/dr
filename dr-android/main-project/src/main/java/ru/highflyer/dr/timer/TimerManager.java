package ru.highflyer.dr.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import ru.highflyer.dr.app.TimerReceiver;
import ru.highflyer.dr.ui.preferences.TicketOrderUtils;

/**
 * Created by alexey.nikitin on 15.09.14.
 */
public class TimerManager {
    private static final long INTERVAL = AlarmManager.INTERVAL_DAY;
//    private static final long INTERVAL = 3000;

    public static void scheduleUnansweredQuestionsPrompt( @NonNull Context context ) {
        PendingIntent pendingIntent = createPendingIntent(context);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        if (TicketOrderUtils.isPassedOnce(context) || TicketOrderUtils.isNotificationsCancelled(context))
            return;

        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + INTERVAL, INTERVAL, pendingIntent);
    }

    private static PendingIntent createPendingIntent( Context context ) {
        Intent intent = new Intent(TimerReceiver.ACTION_TIMER);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static void cancelPrompt( @NonNull Context context ) {
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(createPendingIntent(context));
    }
}
