package ru.highflyer.dr.ui.fragment;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Instrumentation;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.Button;

import ru.highflyer.dr.R;

/**
 * Created by alexey.nikitin on 09.07.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ChooseQuestionFragment extends DialogFragment implements View.OnClickListener {
    public interface ChooseQuestionFragmentListener {
        void onNumberSelected( String number, int requestCode );
    }

    private static final String EXTRA_TITLE_RES_ID = "TITLE_RES_ID";
    private static final String EXTRA_REQUEST_CODE = "REQUEST_CODE";
    private static final String EXTRA_BUTTONS_COUNT = "BUTTONS_COUNT";

    @NonNull
    public static ChooseQuestionFragment newInstance( int titleResourceId, int buttonsCount, int requestCode ) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_TITLE_RES_ID, titleResourceId);
        args.putInt(EXTRA_REQUEST_CODE, requestCode);
        args.putInt(EXTRA_BUTTONS_COUNT, buttonsCount);

        ChooseQuestionFragment result = new ChooseQuestionFragment();
        result.setArguments(args);
        return result;
    }

    private int getTitleResId( ) {
        return getArguments().getInt(EXTRA_TITLE_RES_ID);
    }

    private int getRequestCode( ) {
        return getArguments().getInt(EXTRA_REQUEST_CODE);
    }

    private int getButtonsCount( ) {
        return getArguments().getInt(EXTRA_BUTTONS_COUNT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = View.inflate(getActivity(), R.layout.choose_question, null);

        GridLayout grid = (GridLayout)view.findViewById(R.id.grid);
        for (int i = 0; i < getButtonsCount(); i++) {
            Button button = new Button(getActivity());
            button.setText(String.valueOf(i + 1));
            button.setOnClickListener(this);

            grid.addView(button);
        }

        builder.setView(view);
        builder.setTitle(getTitleResId());

        return builder.create();
    }

    @Override
    public void onClick( View view ) {
        dismiss();

        FragmentActivity activity = getActivity();
        if (activity == null)
            return;

        Button button = (Button)view;
        String number = button.getText().toString();

        ((ChooseQuestionFragmentListener)activity).onNumberSelected(number, getRequestCode());
    }
}
