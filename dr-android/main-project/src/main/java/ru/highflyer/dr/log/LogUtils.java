package ru.highflyer.dr.log;

import android.app.Activity;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import ru.highflyer.dr.app.AppEx;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 20.06.13 16:13
 */
final public class LogUtils {
    final static private String EVENT_DONT_KNOW_CLICKED = "DontKnowClicked";
    final static private String EVENT_QUESTION_ANSWERED = "QuestionAnswered";
    final static private String EVENT_REPEAT_SHOWN = "RepeatShown";
    final static private String EVENT_THANK_YOU_CLICKED = "ThankYouClicked";
    final static private String EVENT_RATE_DIALOG_SHOWN = "RateDialogShown";
    final static private String EVENT_RATE_DIALOG_RATE_CLICKED = "RateDialogRateClicked";
    final static private String EVENT_RATE_DIALOG_LATER_CLICKED = "RateDialogLaterClicked";
    final static private String EVENT_RATE_DIALOG_NEVER_CLICKED = "RateDialogNeverClicked";
    final static private String EVENT_IMAGE_CLICKED = "ImageClicked";
    final static private String EVENT_TOTAL_DURATION = "TotalDuration";
    final static private String EVENT_CHOOSE_QUESTION_CLICKED = "ChooseQuestionClicked";
    final static private String EVENT_QUESTION_CHOSEN = "QuestionChosen";
    final static private String EVENT_NOTIFICATION_SHOWN = "NotificationShown";
    final static private String EVENT_NOTIFICATION_CANCEL = "NotificationCancel";

    final static private String KEY_QUESTION_ID = "QuestionId";
    final static private String KEY_QUESTION_RESULT = "QuestionResult";
    final static private String KEY_IS_RIGHT = "IsRight";
    final static private String KEY_DURATION = "Duration";

    public static void logQuestionAnswered( String questionId, String ticketNumber,
                String questionNumber, boolean right ) {
        Map<String, String> p = new HashMap<>();
        p.put(KEY_QUESTION_RESULT, questionNumber + "/" + String.valueOf(right));
        FlurryAgent.logEvent(LogUtils.EVENT_QUESTION_ANSWERED + "/" + ticketNumber, p);

        Map<String, String> params = new HashMap<>();
        params.put(KEY_QUESTION_ID, questionId);
        params.put(KEY_IS_RIGHT, String.valueOf(right));
        params.put(KEY_QUESTION_RESULT, questionId + "/" + String.valueOf(right));
        FlurryAgent.logEvent(LogUtils.EVENT_QUESTION_ANSWERED, params);
    }

    public static void logDontKnowClicked( String questionId ) {
        Map<String, String> params = new HashMap<>();
        params.put(KEY_QUESTION_ID, questionId);

        FlurryAgent.logEvent(LogUtils.EVENT_DONT_KNOW_CLICKED, params);
    }

    public static void logRepeatDialog( ) {
        FlurryAgent.logEvent(EVENT_REPEAT_SHOWN);
    }

    public static void thankYouClicked( ) {
        FlurryAgent.logEvent(EVENT_THANK_YOU_CLICKED);
    }

    public static void rateDialogShown( ) {
        FlurryAgent.logEvent(EVENT_RATE_DIALOG_SHOWN);
    }

    public static void rateDialogPositiveClicked( ) {
        FlurryAgent.logEvent(EVENT_RATE_DIALOG_RATE_CLICKED);
    }

    public static void rateDialogLaterClicked( ) {
        FlurryAgent.logEvent(EVENT_RATE_DIALOG_LATER_CLICKED);
    }

    public static void rateDialogNeverClicked( ) {
        FlurryAgent.logEvent(EVENT_RATE_DIALOG_NEVER_CLICKED);
    }

    public static void startSession( Activity activity ) {
        FlurryAgent.onStartSession(activity);

        Tracker tracker = AppEx.getTracker(activity);
        tracker.setScreenName(activity.getClass().getName());
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    public static void endSession( Activity activity ) {
        FlurryAgent.onEndSession(activity);
    }

    public static void logImageClicked( String questionId ) {
        Map<String, String> params = new HashMap<>();
        params.put(KEY_QUESTION_ID, questionId);

        FlurryAgent.logEvent(EVENT_IMAGE_CLICKED, params);
    }

    public static void logTotalDuration( long totalDuration ) {
        Map<String, String> params = new HashMap<>();
        params.put(KEY_DURATION, String.valueOf(totalDuration));

        FlurryAgent.logEvent(EVENT_TOTAL_DURATION, params);
    }

    public static void pageView( ) {
        FlurryAgent.onPageView();
    }

    public static void logChooseQuestion() {
        FlurryAgent.logEvent(EVENT_CHOOSE_QUESTION_CLICKED, null);
    }

    public static void logQuestionChosen( String ticketNumber, String questionNumber ) {
        Map<String, String> p = new HashMap<>();
        p.put(KEY_QUESTION_RESULT, questionNumber);
        FlurryAgent.logEvent(EVENT_QUESTION_CHOSEN + "/" + ticketNumber, p);

        FlurryAgent.logEvent(EVENT_QUESTION_CHOSEN, null);
    }

    public static void logShowNotification( ) {
        FlurryAgent.logEvent(EVENT_NOTIFICATION_SHOWN);
    }

    public static void logCancelNotifications() {
        FlurryAgent.logEvent(EVENT_NOTIFICATION_CANCEL);
    }
}
