package ru.highflyer.dr.ui.activity;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.Toast;
import ru.highflyer.dr.R;
import ru.highflyer.dr.db.DRContentProvider;
import ru.highflyer.dr.ui.fragment.QuestionFragment;

/**
 * Created by alexey.nikitin on 09.07.14.
 */
public class FindQuestionIdAsyncTask extends AsyncTask<Pair<String, String>, Void, String> {
    private final FragmentActivity activity;

    public FindQuestionIdAsyncTask( FragmentActivity activity ) {
        this.activity = activity;
    }

    @Override
    protected String doInBackground( Pair<String, String>... pairs ) {
        Pair<String, String> pair = pairs[0];
        String ticketNumber = pair.first;
        String questionNumber = pair.second;

        Cursor cursor = activity.getContentResolver().query(
                DRContentProvider.questionsUri(), new String[] {BaseColumns._ID},
                "ticket_number = ? AND question_number = ?",
                new String[] {ticketNumber, questionNumber}, null);
        if (cursor != null && cursor.moveToFirst())
            return cursor.getString(0);

        return null;
    }

    @Override
    protected void onPostExecute( String questionId ) {
        super.onPostExecute(questionId);

        if (activity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed()))
            return;

        if (!TextUtils.isEmpty(questionId))
            activity.getSupportFragmentManager().beginTransaction().
                    replace(R.id.question, QuestionFragment.newInstance(questionId)).commitAllowingStateLoss();
        else
            Toast.makeText(activity, R.string.select_question_failed, Toast.LENGTH_LONG);
    }
}
