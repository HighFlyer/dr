package ru.highflyer.dr.db;

import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13 11:17 PM
 */
final public class DBHelper extends SQLiteAssetHelper {
    private static final String DB_NAME = "dr";
    // 5 - new comments introduced
    // 6 - statistics updated
    // 7 - statistics updated
    // 8 - all data refetch due to user request
    // 9 - few questions updated, questions without answers introduced
    // 10 - questions updated
    private static final int DB_VERSION = 10;

    public DBHelper( Context context ) {
        super(context, DB_NAME, null, DB_VERSION);
        setForcedUpgradeVersion(DB_VERSION);
    }
}
