package ru.highflyer.dr.app;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import ru.highflyer.dr.R;
import ru.highflyer.dr.log.LogUtils;
import ru.highflyer.dr.timer.TimerManager;
import ru.highflyer.dr.ui.activity.MainActivity;
import ru.highflyer.dr.ui.preferences.TicketOrderUtils;

import java.util.List;

public final class TimerReceiver extends BroadcastReceiver {
    public static final String ACTION_TIMER = "ru.highflyer.dr.app.TIMER_ACTION";
    public static final String ACTION_CANCEL_TIMER = "ru.highflyer.dr.app.TIMER_CANCEL_ACTION";
    public static final int NOTIFICATION_ID = 0;

    private static final String TAG = TimerReceiver.class.getSimpleName();

    public TimerReceiver() {
    }

    @Override
    public void onReceive( Context context, Intent intent ) {
        String action = intent.getAction();
        Log.d(TAG, "onReceive: " + action);

        switch (action) {
        case ACTION_TIMER:
            if (!TicketOrderUtils.isNotificationsCancelled(context)) {
                List<String> orderIds = TicketOrderUtils.getOrderIds(context);
                if (!orderIds.isEmpty()) {
                    LogUtils.logShowNotification();
                    showNotification(context, orderIds.size());
                }
            }
            else
                Log.d(TAG, "Notifications cancelled");

            break;
        case ACTION_CANCEL_TIMER:
            Log.d(TAG, "Cancel notifications");
            LogUtils.logCancelNotifications();
            TicketOrderUtils.setNotificationsCancelled(context);
            TimerManager.cancelPrompt(context);

            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
            managerCompat.cancel(NOTIFICATION_ID);

            break;
        }
    }

    private void showNotification( Context context, int size ) {
        Log.d(TAG, "showNotification");

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle(context.getString(R.string.notification_title));
        builder.setContentText(context.getString(R.string.notification_questions_left, size));
        builder.setSmallIcon(R.drawable.ic_launcher);

        Intent cancelIntent = new Intent(TimerReceiver.ACTION_CANCEL_TIMER);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(
                android.R.drawable.ic_menu_close_clear_cancel,
                context.getResources().getString(R.string.notification_stop),
                cancelPendingIntent);
        builder.addAction(
                android.R.drawable.ic_menu_more,
                context.getResources().getString(R.string.notification_go),
                pendingIntent);

        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.notify(NOTIFICATION_ID, builder.build());
    }
}
