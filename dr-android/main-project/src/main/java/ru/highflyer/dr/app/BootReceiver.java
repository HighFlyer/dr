package ru.highflyer.dr.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import ru.highflyer.dr.timer.TimerManager;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        TimerManager.scheduleUnansweredQuestionsPrompt(context);
    }
}
