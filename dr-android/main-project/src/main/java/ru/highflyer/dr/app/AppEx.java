package ru.highflyer.dr.app;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import ru.highflyer.dr.R;
import ru.highflyer.dr.timer.TimerManager;

/**
 * Created by alexey.nikitin on 27.08.14.
 */
public final class AppEx extends Application {
    private Tracker tracker;

    @Override
    public void onCreate( ) {
        super.onCreate();

        FlurryAgent.init(this, "SKFM4FP9J7N3NZSRPRYW");
        TimerManager.scheduleUnansweredQuestionsPrompt(this);
    }

    public Tracker getTracker() {
        if (tracker == null)
            tracker = GoogleAnalytics.getInstance(this).newTracker(R.xml.analytics);

        return tracker;
    }

    public static Tracker getTracker(@NonNull Context context) {
        AppEx app = (AppEx)context.getApplicationContext();
        return app.getTracker();
    }
}
