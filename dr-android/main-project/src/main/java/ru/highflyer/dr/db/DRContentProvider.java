package ru.highflyer.dr.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13 11:16 PM
 */
final public class DRContentProvider extends ContentProvider {
    private DBHelper helper;

    final static private String AUTHORITY = "ru.highflyer.dr";
    private static final String BASE_PATH = "questions";
    final static private Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    final static private int CODE_ROOT = 0;
    final static private int CODE_QUESTION = 1;
    final static private int CODE_QUESTIONS = 2;
    final static private int CODE_ANSWERS = 3;
    final static private UriMatcher URI_MATCHER = new UriMatcher(CODE_ROOT);

    static {
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH + "/#", CODE_QUESTION);
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH, CODE_QUESTIONS);
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH + "/#/answers", CODE_ANSWERS);
    }

    @Override
    public boolean onCreate( ) {
        helper = new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query( Uri uri, String [] projection, String selection, String [] args, String sortOrder ) {
        String tableName;
        switch (URI_MATCHER.match(uri)) {
        case CODE_QUESTION:
            tableName = "question";
            String questionId = uri.getPathSegments().get(1);
            selection = "_id = ?";
            args = new String [] {questionId};
            break;
        case CODE_QUESTIONS:
            tableName = "question";
            break;
        case CODE_ANSWERS:
            questionId = uri.getPathSegments().get(1);
            selection = "question_id = ?";
            args = new String [] {questionId};
            tableName = "answer";
            break;
        default:
            throw new IllegalArgumentException("Can't match " + uri);
        }

        Cursor result = helper.getReadableDatabase().query(tableName, projection, selection, args, null, null, sortOrder);
        result.setNotificationUri(getContext().getContentResolver(), uri);

        return result;
    }

    @Override
    public String getType( Uri uri ) {
        switch (URI_MATCHER.match(uri)) {
        case CODE_QUESTIONS:
            return ContentResolver.CURSOR_ITEM_BASE_TYPE + ".question";
        }
        return null;
    }

    @Override
    public Uri insert( Uri uri, ContentValues contentValues ) {
        return null;
    }

    @Override
    public int delete( Uri uri, String s, String[] strings ) {
        return 0;
    }

    @Override
    public int update( Uri uri, ContentValues contentValues, String s, String[] strings ) {
        return 0;
    }

    public static Uri questionsUri( ) {
        return CONTENT_URI;
    }

    public static Uri answersUri( String questionId ) {
        return Uri.parse(CONTENT_URI + "/" + questionId + "/answers");
    }

    public static Uri questionUri( String questionId ) {
        return Uri.parse(CONTENT_URI + "/" + questionId);
    }
}
