package ru.highflyer.dr.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import ru.highflyer.dr.R;
import ru.highflyer.dr.log.LogUtils;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 18.02.14 22:52
 */
final public class RateDialogFragment extends DialogFragment {
    private static final String PREFS_RATE_DIALOG = "RateDialogPrefs";

    private static final String KEY_FIRST_LAUNCH_TIME = "first_launch_time";
    private static final String KEY_LAUNCH_COUNT = "launch_count";
    private static final String KEY_SHOWN = "dialog_shown";

    public static final int LAUNCH_COUNT = 4;
    public static final long LAUNCH_DELTA = 3 * 24 * 60 * 60 * 1000L;

    public static void showDialogIfTime( FragmentActivity activity ) {
        if (!shouldShowRateDialog(activity))
            return;

        LogUtils.rateDialogShown();
        new RateDialogFragment().show(activity.getSupportFragmentManager(), "rate-dialog");
    }

    private static boolean shouldShowRateDialog( Context context ) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_RATE_DIALOG, 0);
        if (preferences.getBoolean(KEY_SHOWN, false))
            return false;

        if (!preferences.contains(KEY_FIRST_LAUNCH_TIME)) {
            preferences.edit().putLong(KEY_FIRST_LAUNCH_TIME, System.currentTimeMillis()).commit();
            return false;
        }

        long launchTime = preferences.getLong(KEY_FIRST_LAUNCH_TIME, 0);
        int launchCount = preferences.getInt(KEY_LAUNCH_COUNT, 0);
        preferences.edit().putInt(KEY_LAUNCH_COUNT, launchCount + 1).commit();

        return launchCount >= LAUNCH_COUNT || System.currentTimeMillis() - launchTime >= LAUNCH_DELTA;
    }

    @Override
    public void onCancel( DialogInterface dialog ) {
        super.onCancel(dialog);

        resetStartValues();
    }

    public static boolean wasShown( Context context ) {
        if (context == null)
            return false;

        SharedPreferences preferences = context.getSharedPreferences(PREFS_RATE_DIALOG, 0);
        return preferences.getBoolean(KEY_SHOWN, false);
    }

    private void setShown( ) {
        Activity activity = getActivity();
        if (activity == null)
            return;

        SharedPreferences preferences = activity.getSharedPreferences(PREFS_RATE_DIALOG, 0);
        preferences.edit().putBoolean(KEY_SHOWN, true).commit();
    }

    private void resetStartValues( ) {
        Activity activity = getActivity();
        if (activity == null)
            return;

        SharedPreferences preferences = activity.getSharedPreferences(PREFS_RATE_DIALOG, 0);
        preferences.edit().putInt(KEY_LAUNCH_COUNT, 0).putLong(KEY_FIRST_LAUNCH_TIME, System.currentTimeMillis()).commit();
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.rate_dialog_title);
        builder.setMessage(R.string.rate_dialog_message);
        builder.setPositiveButton(R.string.rate_dialog_button_rate, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                LogUtils.rateDialogPositiveClicked();
                setShown();

                launchMarket(getActivity());
            }
        });
        builder.setNeutralButton(R.string.rate_dialog_button_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                LogUtils.rateDialogLaterClicked();
                resetStartValues();
            }
        });
        builder.setNegativeButton(R.string.rate_dialog_button_never, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                LogUtils.rateDialogNeverClicked();
                setShown();
            }
        });

        return builder.create();
    }

    public static void launchMarket( Context context ) {
        if (context != null)
            context.startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.highflyer.dr")));
    }
}
