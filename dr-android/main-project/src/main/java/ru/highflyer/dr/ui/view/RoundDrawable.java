package ru.highflyer.dr.ui.view;

import android.graphics.*;
import android.graphics.drawable.Drawable;

/**
* Created with IntelliJ IDEA.
* User: alexey
* Date: 11/12/13 23:31
*/
public class RoundDrawable extends Drawable {
    private final RectF mRect = new RectF();
    private final BitmapShader mBitmapShader;
    private final Paint mPaint;
    private final int mMargin;
    private final Bitmap bitmap;
    private final int cornerRadius;

    RoundDrawable( Bitmap bitmap, int margin, int cornerRadius ) {
        mBitmapShader = new BitmapShader(this.bitmap = bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setShader(mBitmapShader);

        mMargin = margin;
        this.cornerRadius = cornerRadius;
    }

    @Override
    protected void onBoundsChange( Rect bounds ) {
        super.onBoundsChange(bounds);
        mRect.set(mMargin, mMargin, bounds.width() - mMargin, bounds.height() - mMargin);

        updateScale();
    }

    private void updateScale( ) {
        float scale = Math.max(mRect.width() / bitmap.getWidth(), mRect.height() / bitmap.getHeight());
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        matrix.postTranslate(
                getBounds().centerX() - bitmap.getWidth() * scale / 2,
                getBounds().centerY() - bitmap.getHeight() * scale / 2);
        mBitmapShader.setLocalMatrix(matrix);
    }

    @Override
    public void draw( Canvas canvas ) {
        Path path = new Path();
        path.moveTo(0, mRect.centerY());
        float r2 = cornerRadius * 2;
        RectF oval1 = new RectF(0, 0, r2, r2);
        RectF oval2 = new RectF(mRect.width() - r2, 0, mRect.width(), r2);

        path.arcTo(oval1, 180, 90);
        path.arcTo(oval2, -90, 90);
        path.lineTo(mRect.width(), mRect.height());
        path.lineTo(0, mRect.height());
        path.close();

        canvas.drawPath(path, mPaint);
    }

    public Bitmap getBitmap( ) {
        return bitmap;
    }

    @Override
    public int getOpacity( ) {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha( int alpha ) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter( ColorFilter cf ) {
        mPaint.setColorFilter(cf);
    }

    @Override
    public int getIntrinsicWidth( ) {
        return bitmap.getWidth();
    }

    @Override
    public int getIntrinsicHeight( ) {
        return bitmap.getHeight();
    }
}
