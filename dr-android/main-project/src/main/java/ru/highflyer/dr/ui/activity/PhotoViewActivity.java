package ru.highflyer.dr.ui.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 03.03.14 22:23
 */
final public class PhotoViewActivity extends Activity {
    public static final String KEY_BITMAP = "bitmap";

    private ImageView imageView;
    private PhotoViewAttacher attacher;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        imageView = new ImageView(this);
        setContentView(imageView);

        imageView.setImageBitmap(getIntent().<Bitmap>getParcelableExtra(KEY_BITMAP));

        attacher = new PhotoViewAttacher(imageView);
    }
}
