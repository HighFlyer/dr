package ru.highflyer.dr.ui.fragment;

import android.app.Activity;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.TypedValue;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import ru.highflyer.dr.R;
import ru.highflyer.dr.db.DRContentProvider;
import ru.highflyer.dr.log.LogUtils;
import ru.highflyer.dr.ui.preferences.TicketOrderUtils;
import ru.highflyer.dr.ui.view.EaseView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/9/13 4:15 PM
 */
final public class QuestionFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener {
    public interface QuestionFragmentListener {
        void onQuestionAnsweredRight( String questionId );
        void onImageClicked( View view, Bitmap image );
        void onShowNextQuestion(String questionId);
    }

    private enum Mode {
        QUESTION,
        ERROR
    }

    final static private String EXTRA_QUESTION_ID = "QUESTION_ID";
    final static private String EXTRA_MODE_ORDINAL = "MODE_ORDINAL";

    final static private int LOADER_QUESTION = 0;
    final static private int LOADER_ANSWERS = 1;

    private String ticketNumber;
    private String questionNumber;
    private String errorComment;

    private ImageView image;
    private TextView question;
    private TextView info;
    private ViewGroup answers;
    private ScrollView dataView;
    private View progress;
    private EaseView easeView;
    private ViewStub errorBlockStub;
    private MenuItem dontKnow;
    private Mode mode = Mode.QUESTION;

    final private View.OnClickListener answerClickListener = new View.OnClickListener() {
        @Override
        public void onClick( View v ) {
            Boolean isCorrect = (Boolean)v.getTag();

            if (isCorrect == null) {
                ((QuestionFragmentListener)getActivity()).onShowNextQuestion(getQuestionId());
            }
            else {
                LogUtils.logQuestionAnswered(getQuestionId(), ticketNumber, questionNumber, isCorrect);

                if (isCorrect) {
                    ((QuestionFragmentListener)getActivity()).onQuestionAnsweredRight(getQuestionId());
                } else {
                    showErrorMessage();
                    TicketOrderUtils.increaseIncorrectNumber(getActivity());
                }
            }
        }
    };

    public static QuestionFragment newInstance( String questionId ) {
        Bundle args = new Bundle();
        args.putString(EXTRA_QUESTION_ID, questionId);

        QuestionFragment result = new QuestionFragment();
        result.setArguments(args);

        return result;
    }

    public String getQuestionId( ) {
        return getArguments().getString(EXTRA_QUESTION_ID);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.question, container, false);
        image = (ImageView)view.findViewById(R.id.image);
        question = (TextView)view.findViewById(R.id.question);
        info = (TextView)view.findViewById(R.id.info);
        answers = (ViewGroup)view.findViewById(R.id.answers_block);
        dataView = (ScrollView)view.findViewById(R.id.data_block);
        progress = view.findViewById(R.id.progress);
        easeView = (EaseView)view.findViewById(R.id.ease);
        errorBlockStub = (ViewStub)view.findViewById(R.id.error_block_stub);

        image.setOnClickListener(this);

        getLoaderManager().restartLoader(LOADER_QUESTION, null, this);
        getLoaderManager().restartLoader(LOADER_ANSWERS, null, this);

        if (savedInstanceState != null) {
            int ordinal = savedInstanceState.getInt(EXTRA_MODE_ORDINAL, Mode.QUESTION.ordinal());
            mode = Mode.values()[ordinal];
        }
        else
            mode = Mode.QUESTION;

        return view;
    }

    @Override
    public void onSaveInstanceState( Bundle outState ) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_MODE_ORDINAL, mode.ordinal());
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
        inflater.inflate(R.menu.question, menu);
        dontKnow = menu.findItem(R.id.dont_know);
        dontKnow.setEnabled(mode != Mode.ERROR);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onConfigurationChanged( Configuration newConfig ) {
        super.onConfigurationChanged(newConfig);

        getActivity().supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
        case R.id.dont_know:
            showErrorMessage();
            if (mode != Mode.ERROR)
                logDontKnowClick();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void logDontKnowClick( ) {
        List<String> ids = TicketOrderUtils.getOrderIds(getActivity());
        if (!ids.isEmpty())
            LogUtils.logDontKnowClicked(ids.get(0));
    }

    @Override
    public Loader<Cursor> onCreateLoader( int id, Bundle bundle ) {
        switch (id) {
        case LOADER_QUESTION: {
            return new CursorLoader(getActivity(), DRContentProvider.questionUri(getQuestionId()), null, null, null, null);
        }
        case LOADER_ANSWERS: {
            return new CursorLoader(getActivity(), DRContentProvider.answersUri(getQuestionId()), null, null, null, "RANDOM()");
        }
        default:
            throw new IllegalArgumentException("Don't know " + id + " id");
        }
    }

    @Override
    public void onLoadFinished( Loader loader, Cursor c ) {
        switch (loader.getId()) {
        case LOADER_QUESTION:
            questionLoaded(c);
            break;
        case LOADER_ANSWERS:
            answersLoaded(c);
            break;
        }
    }

    private void answersLoaded( Cursor c ) {
        boolean buttonEnabled = mode == Mode.QUESTION;

        if (c.getCount() == 0) {
            Button button = createButton(answers);
            button.setText(getString(R.string.next_question));

            answers.addView(button);
        }
        else
            while (c.moveToNext()) {
                Button button = createButton(answers);
                button.setEnabled(buttonEnabled);
                button.setText(c.getString(c.getColumnIndex("_text")));
                button.setTag(c.getInt(c.getColumnIndex("is_correct")) > 0);

                answers.addView(button);

                if (!c.isLast()) {
                    View separator = LayoutInflater.from(getActivity()).inflate(R.layout.answer_separator, answers, false);

                    answers.addView(separator);
                }
            }
        hideProgress();
    }

    private Button createButton(ViewGroup parent) {
        Button button = (Button)LayoutInflater.from(getActivity()).inflate(R.layout.answer_button, parent, false);
        button.setOnClickListener(answerClickListener);
        return button;
    }

    private void questionLoaded( Cursor c ) {
        if (!c.moveToFirst())
            return;

        byte [] imageData = c.getBlob(c.getColumnIndex("image_data"));
        if (imageData != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
            image.setImageBitmap(bitmap);
        }
        else
            image.setImageBitmap(null);

        question.setText(c.getString(c.getColumnIndex("_text")));
        int padding = (int)TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
        question.setPadding(padding, 2 * padding / 4, padding, padding);

        ticketNumber = c.getString(c.getColumnIndex("ticket_number"));
        questionNumber = c.getString(c.getColumnIndex("question_number"));
        info.setText(getString(R.string.question_info, ticketNumber, questionNumber));
        easeView.setEase(c.getFloat(c.getColumnIndex("ease")));

        errorComment = c.getString(c.getColumnIndex("comment"));

        if (mode == Mode.ERROR)
            updateErrorUI();
    }

    @Override
    public void onLoaderReset( Loader loader ) {
    }

    private void hideProgress( ) {
        progress.setVisibility(View.GONE);
        dataView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick( View v ) {
        Activity activity = getActivity();
        if (activity == null)
            return;

        if (v == image) {
            LogUtils.logImageClicked(getQuestionId());
            ((QuestionFragmentListener)activity).onImageClicked(image,
                    ((BitmapDrawable)image.getDrawable()).getBitmap());
        }
        else if (v.getId() == R.id.error_confirm)
          ((QuestionFragmentListener)activity).onShowNextQuestion(getQuestionId());
    }

    private void showErrorMessage( ) {
        mode = Mode.ERROR;

        updateErrorUI();
    }

    private void updateErrorUI() {
        if (errorBlockStub != null) {
            errorBlockStub.inflate();
            errorBlockStub = null;
        }

        TextView errorMessage = (TextView)getView().findViewById(R.id.error_message);
        View confirmButton = getView().findViewById(R.id.error_confirm);
        confirmButton.setOnClickListener(this);

        dataView.post(new Runnable() {
            @Override
            public void run() {
                View v = getView().findViewById(R.id.error_block);
                dataView.smoothScrollTo(0, v.getTop());
            }
        });

        errorMessage.setText(errorComment);

        for (int i = 0; i < answers.getChildCount(); i++) {
            View view = answers.getChildAt(i);
            view.setEnabled(false);
        }

        if (dontKnow != null)
            dontKnow.setEnabled(false);
    }
}
