package ru.highflyer.dr.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import ru.highflyer.dr.R;
import ru.highflyer.dr.log.LogUtils;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/11/13 10:18 PM
 */
final public class RepeatFragment extends DialogFragment {
    public interface RepeatFragmentListener {
        void onRepeatFragmentClosed( );
    }

    public static RepeatFragment newInstance( ) {
        Bundle args = new Bundle();

        RepeatFragment result = new RepeatFragment();
        result.setArguments(args);
        return result;
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.
                setNegativeButton(R.string.good, null).
                setTitle(R.string.congratulations_title).
                setMessage(R.string.congratulations_message);

        if (!RateDialogFragment.wasShown(getActivity()))
            builder.setPositiveButton(R.string.thank_you, new DialogInterface.OnClickListener() {
                @Override
                public void onClick( DialogInterface dialog, int which ) {
                    LogUtils.thankYouClicked();
                    RateDialogFragment.launchMarket(getActivity());
                }
            });

        return builder.create();
    }

    @Override
    public void onDismiss( DialogInterface dialog ) {
        super.onDismiss(dialog);

        ((RepeatFragmentListener)getActivity()).onRepeatFragmentClosed();
    }
}
