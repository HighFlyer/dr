package ru.highflyer.dr.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/9/13 9:19 AM
 */
final public class AspectRatioImageView extends ImageView {
    public AspectRatioImageView( Context context, AttributeSet attrs ) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (getDrawable() == null || getDrawable().getIntrinsicWidth() <= 0)
            return;

        int width = getMeasuredWidth();
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();

        setMeasuredDimension(width, height);
    }
}
