package ru.highflyer.dr.importer.dto.flurry;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:27
 */
final public class KeyParameter {
    @JsonProperty("@name")
    public String name;
    @JsonProperty("value")
    public List<ValueParameter> value;
}
