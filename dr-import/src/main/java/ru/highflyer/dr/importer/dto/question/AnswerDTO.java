package ru.highflyer.dr.importer.dto.question;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 7/29/13 10:25 PM
 */
final public class AnswerDTO {
    private final String text;
    private final boolean isCorrect;

    public AnswerDTO( String text, boolean correct ) {
        this.text = text;
        isCorrect = correct;
    }

    public String getText( ) {
        return text;
    }

    public boolean isCorrect( ) {
        return isCorrect;
    }
}
