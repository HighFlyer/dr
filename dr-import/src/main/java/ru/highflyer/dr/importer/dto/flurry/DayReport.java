package ru.highflyer.dr.importer.dto.flurry;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:30
 */
final public class DayReport {
    @JsonProperty("@date")
    public String date;
    @JsonProperty("@totalCount")
    public int totalCount;
    @JsonProperty("@totalSessions")
    public int totalSessions;
    @JsonProperty("@uniqueUsers")
    public int uniqueUsers;
}
