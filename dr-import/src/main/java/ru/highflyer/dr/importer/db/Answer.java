package ru.highflyer.dr.importer.db;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Answer {
    @Id
    @GeneratedValue
    @Column(name = "_id")
    private int _id;

    @Column(name = "_text")
    private String text;
    @Column(name = "is_correct")
    private boolean isCorrect;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    public Answer( String text, boolean isCorrect ) {
        this.text = text;
        this.isCorrect = isCorrect;
    }

    public Answer( ) {
    }

    public String getText() {
        return text;
    }

    public void setQuestion( Question question ) {
        this.question = question;
    }
}
