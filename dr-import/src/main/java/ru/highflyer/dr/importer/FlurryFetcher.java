package ru.highflyer.dr.importer;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ru.highflyer.dr.importer.dto.flurry.KeyParameter;
import ru.highflyer.dr.importer.dto.flurry.SummaryReport;
import ru.highflyer.dr.importer.dto.flurry.ValueParameter;
import ru.highflyer.dr.importer.model.QuestionStatistics;
import ru.highflyer.dr.importer.model.QuestionsData;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 11.10.13 10:23
 */
final public class FlurryFetcher {
    private static final List<HttpMessageConverter<?>> messageConverters;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        MappingJacksonHttpMessageConverter messageConverter = new MappingJacksonHttpMessageConverter();
        messageConverter.setObjectMapper(mapper);

        messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(messageConverter);
    }

    static QuestionsData fetchFlurryStatistics() {
        List<String> apiKeys = Arrays.asList(
                "D48NTC6K79G32W86NFBP", // ios
                "SKFM4FP9J7N3NZSRPRYW"); // android

        QuestionsData questionsData = new QuestionsData();
        for (String apiKey : apiKeys)
            processApiKey(questionsData, apiKey);

        Collections.sort(questionsData.statistics, new Comparator<QuestionStatistics>() {
            @Override
            public int compare( QuestionStatistics a, QuestionStatistics b ) {
                int result = Integer.valueOf(a.ticketNumber).compareTo(b.ticketNumber);
                if (result != 0)
                    return result;

                return Integer.valueOf(a.questionNumber).compareTo(b.questionNumber);
            }
        });

        float minPercent = 1;
        float maxPercent = 0;
        int i = 0;
        int totalEvents = 0;
        int totalErrors = 0;
        for (QuestionStatistics statistics : questionsData.statistics) {
            float percent = (float)statistics.correctNumber / (statistics.correctNumber + statistics.incorrectNumber);
            totalEvents += statistics.correctNumber + statistics.incorrectNumber;
            totalErrors += statistics.incorrectNumber;
            if (percent < minPercent)
                minPercent = percent;
            if (percent > maxPercent)
                maxPercent = percent;

            System.out.println(i++ + " question: " + statistics.ticketNumber + "." + statistics.questionNumber + ", " + 100f * percent + "% (" + (statistics.correctNumber + statistics.incorrectNumber) + ")");
        }

        System.out.println("Events: errors: " + totalErrors + " / total:" + totalEvents + ", min: " + minPercent + ", max: " + maxPercent);
        questionsData.minPositivePercent = minPercent;
        questionsData.maxPositivePercent = maxPercent;

        return questionsData;
    }

    private static void processApiKey( QuestionsData questionsData, String apiKey ) {
        for (int i = 1; i <= 40; i++) {
            processTicket(i, questionsData, apiKey);
            try {
                /* Flurry allows 1 request per second */
                Thread.sleep(1100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void processTicket( int ticketNumber, QuestionsData questionsData, String apiKey ) {
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 180)); // 180 days before
        String toDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        RestTemplate template = new RestTemplate();
        template.setMessageConverters(messageConverters);
        String url = "http://api.flurry.com/eventMetrics/Event?apiAccessCode=Q35H2SR8JSG96M9B7WZH&" +
                "apiKey=" + apiKey + "&startDate=" + startDate + "&endDate=" + toDay + "&eventName=QuestionAnswered/" + ticketNumber;

        System.out.println("Flurry... " + url);
        SummaryReport report = template.getForObject(url, SummaryReport.class);

        System.out.println("Process report");

        KeyParameter key = null;
        if (report.parameters != null)
            for (KeyParameter keyParameter : report.parameters.key) {
                if ("QuestionResult".equals(keyParameter.name)) {
                    key = keyParameter;
                    break;
                }
            }

        if (key == null) {
            System.err.println("QuestionResult not found!!!");
            return;
        }

        for (ValueParameter value : key.value) {
            String [] chunks = value.name.split("/");
            if (chunks.length != 2) {
                System.err.println("Strange value: " + value.name);
                continue;
            }

            Integer questionNumber = Integer.parseInt(chunks[0]);
            Boolean isCorrect = Boolean.parseBoolean(chunks[1]);

            QuestionStatistics questionStatistics = questionsData.findStatistics(ticketNumber, questionNumber);
            if (questionStatistics == null) {
                questionStatistics = new QuestionStatistics(ticketNumber, questionNumber);
                questionsData.statistics.add(questionStatistics);
            }

            if (isCorrect)
                questionStatistics.correctNumber += value.totalCount;
            else
                questionStatistics.incorrectNumber += value.totalCount;
        }
    }
}
