package ru.highflyer.dr.importer.dto.flurry;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:00
 */
final public class SummaryReport {
    @JsonProperty("@startDate")
    public String startDate;
    @JsonProperty("@endDate")
    public String endDate;
    @JsonProperty("@eventName")
    public String eventName;
    @JsonProperty("@type")
    public String type;
    @JsonProperty("@generatedDate")
    public String generatedDate;
    @JsonProperty("@version")
    public String version;
    @JsonProperty("parameters")
    public ParametersReport parameters;
    @JsonProperty("day")
    public List<DayReport> day;
    @JsonProperty("@versionName")
    public String versionName;
}
