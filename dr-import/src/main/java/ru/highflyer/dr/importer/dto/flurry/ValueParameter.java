package ru.highflyer.dr.importer.dto.flurry;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:28
 */
final public class ValueParameter {
    @JsonProperty("@totalCount")
    public int totalCount;
    @JsonProperty("@name")
    public String name;
}
