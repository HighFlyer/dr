package ru.highflyer.dr.importer.dto.question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 7/29/13 10:23 PM
 */
final public class QuestionDTO {
    private final String comment;
    private final int ticketNumber;
    private final int questionNumber;
    private final String text;
    private final String imageUrl;
    private final byte [] imageData;

    private final List<AnswerDTO> answers = new ArrayList<AnswerDTO>();

    public QuestionDTO( String comment, int ticketNumber, int questionNumber, String text, String imageUrl, byte []
            imageData ) {
        this.comment = comment;
        this.ticketNumber = ticketNumber;
        this.questionNumber = questionNumber;
        this.text = text;
        this.imageUrl = imageUrl;
        this.imageData = imageData;
    }

    public String getComment( ) {
        return comment;
    }

    public int getTicketNumber( ) {
        return ticketNumber;
    }

    public int getQuestionNumber( ) {
        return questionNumber;
    }

    public String getText( ) {
        return text;
    }

    public String getImageUrl( ) {
        return imageUrl;
    }

    public byte [] getImageData( ) {
        return imageData;
    }

    public List<AnswerDTO> getAnswers( ) {
        return answers;
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionDTO that = (QuestionDTO)o;

        if (questionNumber != that.questionNumber) return false;
        if (ticketNumber != that.ticketNumber) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ticketNumber;
        result = 31 * result + questionNumber;
        return result;
    }
}
