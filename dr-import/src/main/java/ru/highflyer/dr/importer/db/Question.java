package ru.highflyer.dr.importer.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13 12:48 PM
 */
@Entity
@NamedQuery(name = Question.QUERY_QUESTION_BY_TICKET, query = "SELECT q FROM Question q WHERE q.questionNumber = :questionNumber AND q.ticketNumber = :ticketNumber")
public class Question {
    public static final String QUERY_QUESTION_BY_TICKET = "QUERY_QUESTION";

    @Id
    @GeneratedValue
    @Column(name = "_id")
    private int _id;

    @Column(name = "question_number")
    private int questionNumber;
    @Column(name = "ticket_number")
    private int ticketNumber;
    @Column(name = "_text")
    private String text;
    @Column(name = "image_data")
    private byte [] imageData;
    @Column(name = "comment")
    private String comment;
    @Column(name = "ease")
    private String ease;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "question")
    private List<Answer> answers;

    public int getId( ) {
        return _id;
    }

    public void setTicketNumber( int ticketNumber ) {
        this.ticketNumber = ticketNumber;
    }

    public void setQuestionNumber( int questionNumber ) {
        this.questionNumber = questionNumber;
    }

    public void setText( String text ) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setImageData( byte [] imageData ) {
        this.imageData = imageData;
    }

    public void setComment( String comment ) {
        this.comment = comment;
    }

    public void setEase( String ease ) {
        this.ease = ease;
    }

    public List<Answer> getAnswers( ) {
        if (answers == null)
            answers = new ArrayList<Answer>();

        return answers;
    }

    public String getComment() {
        return comment;
    }
}
