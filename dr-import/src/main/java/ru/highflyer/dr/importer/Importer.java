package ru.highflyer.dr.importer;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;
import org.xml.sax.SAXException;
import ru.highflyer.dr.importer.db.Answer;
import ru.highflyer.dr.importer.db.Question;
import ru.highflyer.dr.importer.dto.question.AnswerDTO;
import ru.highflyer.dr.importer.dto.question.QuestionDTO;
import ru.highflyer.dr.importer.model.QuestionStatistics;
import ru.highflyer.dr.importer.model.QuestionsData;

import javax.persistence.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: alexey
 * Date: 6/8/13 11:42 AM
 */
final public class Importer {
    public static void main( String [] args ) throws Exception {
        QuestionsData questionsData = FlurryFetcher.fetchFlurryStatistics();
//        QuestionsData questionsData = new QuestionsData();
        List<QuestionDTO> questions = fetchQuestions();
        validateData(questions);
        saveQuestionsInDB(questions, questionsData);
    }

    private static void validateData( List<QuestionDTO> questions ) {
        for (QuestionDTO question : questions)
            validateQuestion(question);
    }

    private static void validateQuestion( QuestionDTO question ) {
        boolean specialQuestion = question.getTicketNumber() == 28 && question.getQuestionNumber() == 5;
        specialQuestion |= question.getTicketNumber() == 37 && question.getQuestionNumber() == 17;
        specialQuestion |= question.getTicketNumber() == 40 && question.getQuestionNumber() == 2;

        if (question.getComment().length() <= 0 && !specialQuestion)
            throw new RuntimeException(question.getTicketNumber() + " - " + question.getQuestionNumber());

        if (question.getText().length() <= 0 && !specialQuestion)
            throw new RuntimeException(question.getTicketNumber() + " - " + question.getQuestionNumber());

        int correctAnswers = 0;
        for (AnswerDTO answer : question.getAnswers()) {
            if (answer.getText().length() <= 0)
                throw new RuntimeException();

            correctAnswers += answer.isCorrect() ? 1 : 0;
        }

        if (correctAnswers != 1 && !specialQuestion)
            throw new RuntimeException(question.getTicketNumber() + " - " + question.getQuestionNumber());
    }

    private static List<QuestionDTO> fetchQuestions( ) throws Exception {
        final int threadsCount = 4;
        final int ticketsCount = 40;

        ExecutorService executor = Executors.newCachedThreadPool();
        final CountDownLatch countDownLatch = new CountDownLatch(threadsCount);
        List<Future<List<QuestionDTO>>> futures =new ArrayList<>();
        for (int i = 0; i < threadsCount; i++) {
            final int finalI = i;
            futures.add(executor.submit(new Callable<List<QuestionDTO>>() {
                @Override
                public List<QuestionDTO> call() throws Exception {
                    try {
                        List<QuestionDTO> questions = new ArrayList<>();
                        int startTicket = 1 + ticketsCount / threadsCount * finalI;
                        int endTicket = startTicket + ticketsCount / threadsCount - 1;

                        for (int ticketNumber = startTicket; ticketNumber <= endTicket; ticketNumber++)
                            for (int questionNumber = 1; questionNumber <= 20; questionNumber++)
                                questions.add(loadOneQuestion(ticketNumber, questionNumber));

                        System.out.println("I'm done: " + finalI);

                        return questions;
                    } finally {
                        countDownLatch.countDown();
                    }
                }
            }));
        }

        countDownLatch.await();
        executor.shutdown();

        List<QuestionDTO> result = new ArrayList<>();
        for (Future<List<QuestionDTO>> future : futures) {
            result.addAll(future.get());
        }

        Set<QuestionDTO> resultSet = new HashSet<>(result);

        System.out.println("Total questions: " + resultSet.size());
        Thread.sleep(2000);

        return result;
    }

    private static QuestionDTO loadOneQuestion( int ticketNumber, int questionNumber )
            throws Exception {
        String address = String.format("http://www.gibdd.ru/mens/avtovladeltsam/exm/ab/%02d/%02d.htm", ticketNumber, questionNumber);
        System.out.print(String.format("Process '%s'...\n", address));
        URL url = new URL(address);
        URLConnection urlConnection = url.openConnection();
        String data = connection2String(urlConnection);
//        System.out.print(" Loaded... ");

        Document dom = createDOM(data);
//        System.out.print(" Parsed... ");

        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        String questionText = xPath.evaluate("//body/center/h2/text()", dom);
        if (questionText == null || questionText.length() == 0)
            questionText = xPath.evaluate("//body/p/text()", dom);

        String imageUrl = xPath.evaluate("//body/center/img/@src", dom);
        byte [] imageData = null;
        if (imageUrl.length() > 0) {
            imageUrl = String.format("http://www.gibdd.ru/mens/avtovladeltsam/exm/ab/%02d/%s", ticketNumber, imageUrl);

            URL imageU = new URL(imageUrl);
            imageData = connection2Bytes(imageU.openConnection());
        }

        QuestionDTO result = new QuestionDTO(fetchComment(ticketNumber, questionNumber), ticketNumber, questionNumber, questionText, /*imageUrl*/null, imageData);

        NodeList answers = (NodeList)xPath.evaluate("//body/ol/li/a", dom, XPathConstants.NODESET);

        for (int i = 0; i < answers.getLength(); i++) {
            Node item = answers.item(i);
            NamedNodeMap attributes = item.getAttributes();
            boolean isCorrect = false;
            for (int j = 0; j < attributes.getLength(); j++) {
                Node href = attributes.item(j);
                if (href.getLocalName().equals("href")) {
                    isCorrect = href.getNodeValue().substring(2, 3).equals("1");
                    break;
                }
            }

            String text = item.getChildNodes().item(0).getNodeValue();
            if (text.length() <= 0)
                text = item.getChildNodes().item(0).getChildNodes().item(0).getNodeValue();

            result.getAnswers().add(new AnswerDTO(text, isCorrect));
        }

        validateQuestion(result);
//        System.out.print(" Validated...");
//        System.out.println(" Done!");

        return result;
    }

     static final Map<String, String> pages = new ConcurrentHashMap<>();

    private static String fetchComment( int ticketNumber, int questionNumber ) throws Exception {
        // http://1pdd.ru/pddib/
        String url = String.format("http://avto-russia.ru/pdd/bilet%d.html?q=%d", ticketNumber, questionNumber);
        String page = pages.get(url);
        if (page == null) {
            page = connection2String(new URL(url).openConnection());
            pages.put(url, page);
        }

        String startString = String.format("qHints[%d]='", questionNumber - 1);
        int startIndex = page.indexOf(startString);
        int lastIndex = page.indexOf("';", startIndex + startString.length());
        if (startIndex < 0 || lastIndex < 0)
            throw new RuntimeException("Failed to find comment");

        String comment = page.substring(startIndex + startString.length(), lastIndex);
        comment = comment.replaceAll("\\<.*?>","");

        return comment;
    }

    private static Document createDOM( String data ) throws ParserConfigurationException, SAXException, IOException {
        Tidy tidy = new Tidy();
        tidy.setInputEncoding("UTF-8");
        tidy.setOutputEncoding("UTF-8");
        tidy.setShowErrors(0);
        tidy.setOnlyErrors(true);
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        return tidy.parseDOM(new ByteArrayInputStream(data.getBytes()), null);
    }

    private static String connection2String( URLConnection connection ) throws IOException {
        return new String(connection2Bytes(connection), "windows-1251");
    }

    private static String connection2StringUTF( URLConnection connection ) throws IOException {
        return new String(connection2Bytes(connection), "UTF-8");
    }

    private static byte [] connection2Bytes( URLConnection connection ) throws IOException {
        InputStream is = new BufferedInputStream(connection.getInputStream());
        ByteArrayBuffer buffer = new ByteArrayBuffer();

        int c;
        while ((c = is.read()) != -1)
            buffer.write(c);

        is.close();

        return buffer.getRawData();
    }

    private static void saveQuestionsInDB( List<QuestionDTO> questions, QuestionsData questionsData ) throws IOException {
        System.out.println("Persisting started");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("dr");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();

            for (QuestionDTO question : questions)
                processOneQuestion(em, question, questionsData);

            tx.commit();

            System.out.println("Persisting finished");
        } finally {
            em.close();
            emf.close();
        }
    }

    private static void processOneQuestion( EntityManager em, QuestionDTO questionXml, QuestionsData questionsData ) throws IOException {
        Question q;
        int ticketNumber = questionXml.getTicketNumber();
        int questionNumber = questionXml.getQuestionNumber();
        try {
            q = (Question)em.createNamedQuery(Question.QUERY_QUESTION_BY_TICKET).
                    setParameter("questionNumber", questionNumber).
                    setParameter("ticketNumber", ticketNumber).getSingleResult();

            compareQuestionVersions(q, questionXml);

            for (Answer answer : q.getAnswers())
                em.remove(answer);

            q.getAnswers().clear();
        } catch (NoResultException e) {
            System.err.println("Question not found: " + questionNumber + " - " + ticketNumber);
            throw new RuntimeException(String.format("Ticket: %s, question: %s", ticketNumber, questionNumber));
        }

        q.setComment(questionXml.getComment());
        q.setImageData(questionXml.getImageData());
//        q.setImageUrl(questionXml.getImageUrl());
        q.setQuestionNumber(questionNumber);
        q.setText(questionXml.getText());

        q.setTicketNumber(ticketNumber);
        q.setEase(String.valueOf(computeEase(questionsData, questionXml)));

        for (AnswerDTO answer : questionXml.getAnswers()) {
            Answer a = new Answer(answer.getText(), answer.isCorrect());
            a.setQuestion(q);
            q.getAnswers().add(a);
        }

        em.persist(q);
    }

    private static void compareQuestionVersions(Question questionOld, QuestionDTO questionNew) {
        List<Answer> oldAnswers = questionOld.getAnswers();
        int ticketNumber = questionNew.getTicketNumber();
        int questionNumber = questionNew.getQuestionNumber();

        if (oldAnswers.size() != questionNew.getAnswers().size())
            System.err.println("\n" + ticketNumber + "-" + questionNumber + " answers count different!");

        for (AnswerDTO newAnswer : questionNew.getAnswers()) {
            String newText = newAnswer.getText();
            boolean found = false;
            for (Answer oldAnswer : oldAnswers) {
                if (oldAnswer.getText().equals(newText)) {
                    found = true;
                    break;
                }
            }
            if (!found)
                System.err.println("\nCan't find corresponding answer " + ticketNumber + "-" + questionNumber + " newText: " + newText);
        }

        String oldText = questionOld.getText();
        String newText = questionNew.getText();

        if (!newText.equals(oldText)) {
            System.err.println(String.format("\nQuestion changed: %d-%d:\nold:'%s'\nnew:'%s'", ticketNumber,
                    questionNumber, oldText, newText));
        }

        String oldComment = questionOld.getComment();
        String newComment = questionNew.getComment();

        if (!newComment.equals(oldComment)) {
            System.err.println(String.format("\nComment changed: %d-%d:\nold:'%s'\nnew:'%s'", ticketNumber,
                    questionNumber, oldComment, newComment));
        }
    }

    private static float computeEase( QuestionsData questionsData, QuestionDTO q ) {
        QuestionStatistics stats = questionsData.findStatistics(q.getTicketNumber(), q.getQuestionNumber());
        float ease;
        if (stats == null)
            throw new RuntimeException("Stats missing: " + q.getTicketNumber() + " - " + q.getQuestionNumber());

        float questionPercent = (float)stats.correctNumber / (stats.correctNumber + stats.incorrectNumber);
        ease = (questionPercent - questionsData.minPositivePercent) / (questionsData.maxPositivePercent -
                questionsData.minPositivePercent);

        return ease;
    }
}
