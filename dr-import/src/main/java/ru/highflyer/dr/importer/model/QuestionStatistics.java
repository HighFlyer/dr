package ru.highflyer.dr.importer.model;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:38
 */
final public class QuestionStatistics {
    public final int ticketNumber;
    public final int questionNumber;
    public int correctNumber;
    public int incorrectNumber;

    public QuestionStatistics( int ticketNumber, int questionNumber ) {
        this.ticketNumber = ticketNumber;
        this.questionNumber = questionNumber;
    }
}
