package ru.highflyer.dr.importer.dto.flurry;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:35
 */
final public class ParametersReport {
    @JsonProperty("key")
    public List<KeyParameter> key;
}
