package ru.highflyer.dr.importer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alexey.nikitin
 * Date: 29.09.13 8:57
 */
final public class QuestionsData {
    public float minPositivePercent;
    public float maxPositivePercent;

    public List<QuestionStatistics> statistics = new ArrayList<QuestionStatistics>();

    public QuestionStatistics findStatistics( int ticketNumber, int questionNumber ) {
        for (QuestionStatistics statistic : statistics)
            if (statistic.questionNumber == questionNumber && statistic.ticketNumber == ticketNumber)
                return statistic;

        return null;
    }
}
