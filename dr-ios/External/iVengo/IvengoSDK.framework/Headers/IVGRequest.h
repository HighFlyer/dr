//
//  IVGRequest.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

/**
 *  Класс описывающий запрос рекламы. Позволяет указать дополнительные параметры запроса
 *  
 *  @note В текущей реализации доступен только метод создания запроса по-умолчанию.
 *  В дальнейшем класс будет расширяться дополнительными параметрами запроса.
 */
@interface IVGRequest : NSObject

/**
 *  @return Возвращает новый объект запроса рекламы
 */
+ (instancetype)defaultRequest;

/**
 *  Метод позволяет задать географические координаты, которые могут помочь получить более таргетированную рекламу
 *
 *  @param latitude         широта
 *  @param longitude        долгота
 *  @param accuracyInMeters точность определения координат (в метрах)
 */
- (void)setLocationWithLatitude:(CGFloat)latitude
                      longitude:(CGFloat)longitude
                       accuracy:(CGFloat)accuracyInMeters;

/**
 *  @return Метод позволяет задать свой идентификатор приложения для каждого запроса
 *  @param appId            идентификатор приложения
 */
- (void)setAppID:(NSString *)appId;

@end
