//
//  IVGAdViewDelegate.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IVGAdView;
@class IVGError;

/**
 *  Протокол объекта-делегата рекламного места IVGAdView. Получает уведомления об изменении состояния рекламного места
 */
@protocol IVGAdViewDelegate <NSObject>
@optional

/**
 *  Метод вызывается когда IVGAdView получило рекламу. В этот момент можно добавить реклманое место в иерархию представлений,
 *  если оно не было добавлено ранее
 *
 *  @param adView баннерное место
 */
- (void)adViewDidReceiveAd:(IVGAdView *)adView;

/**
 *  Метод вызывается в случае проблем с получением рекламы либо с её отображением.
 *  Например, если интернет соединение недоступно или размер баннерного места уменьшился так, что полученная реклама не может быть отображена.
 *
 *  @param adView баннерное место
 *  @param error  объект содержащий описание ошибки
 */
- (void)adView:(IVGAdView *)adView didFailWithError:(IVGError *)error;

/**
 *  Метод вызывается в момент начала отображения рекламы пользователю. Для видеорекламы - это момент старта проигрывания ролика.
 *
 *  @param adView баннерное место
 */
- (void)adViewDidShowAd:(IVGAdView *)adView;

/**
 *  Метод вызывается, когда пользователь сам закрыл рекламу. Кнопка "Пропустить" в видеоролике или кнопка закрытия в баннере.
 *
 *  @param adView баннерное место
 */
- (void)adViewDidSkipAd:(IVGAdView *)adView;

/**
 *  Метод вызывается, когда рекламное место было закрыто самим сдк. Например закончился видеоролик или истёк таймаут показа баннера.
 *
 *  @param adView баннерное место
 */
- (void)adViewDidFinishAd:(IVGAdView *)adView;

/**
 *  Метод вызывается, когда пользователь переходит по рекламной ссылке и запускается другое приложение, например браузер Safari
 *
 *  @param adView баннерное место
 *  @param url    ссылка по которой переходит пользователь
 */
- (void)adView:(IVGAdView *)adView willLeaveApplicationWithURL:(NSURL *)url;

/**
 *  Метод вызывается, когда пользователь возвращается в приложение после перехода по ссылке
 *
 *  @param adView баннерное место
 */
- (void)adViewWillReturnToApplication:(IVGAdView *)adView;

@end
