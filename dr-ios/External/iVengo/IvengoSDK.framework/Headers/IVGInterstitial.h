//
//  IVGInterstitial.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "IVGAdType.h"
#import "IVGInterstitialDelegate.h"

@class IVGRequest;

/**
 *  Класс описывающий полноэкранную рекламу
 */
@interface IVGInterstitial : NSObject

/**
 *  Обязательное свойство указывающее типоразмер рекламы, которая должна отображаться для этого баннерного места
 *  Типоразмер обязан быть полноэкранным, т.е. IVGAdTypeIsInterstitial для него должен возвращать YES
 */
@property(nonatomic, assign) IVGAdType adType;

/**
 *  Опциональное свойство рекламного места, для разделения статистики в личном кабинете.
 */
@property(nonatomic, copy) NSString *adPlace;

/**
 *  Опциональное свойство. Объект который получает все изменения состояния рекламы описанные в IVGInterstitialDelegate
 */
@property(nonatomic, weak) id <IVGInterstitialDelegate>delegate;

/**
 *  Если значение isReady равно YES - реклама готова к отображению. 
 *  Метод делегата -interstitialDidReceiveAd: вызывается в момент установки этого свойства в значение YES
 */
@property(nonatomic, readonly, assign) BOOL isReady;

/**
 *  Конструктор полноэкранной рекламы
 *
 *  @param type типоразмер рекламы (обязан быть одним из kIVGAdTypeBannerInterstitial, kIVGAdTypeVideoInterstitial)
 *
 *  @return возвращает инициализированный объект рекламы либо nil если передан неправильный аргумент
 */
- (id)initWithAdType:(IVGAdType)type;

/**
 *  Метод запускает загрузку рекламы с указанным объектом запроса
 *
 *  @param request объект запроса. В большинстве случаев нужно передавать [IVGRequest defaultRequest]
 */
- (void)loadRequest:(IVGRequest *)request;

/**
 *  Метод отображает рекламу в отдельном полноэкранном view controller'е. Если реклама находится не в состоянии isReady - показ не произойдёт
 *
 *  @param rootViewController корневой контроллер, который будет отображать контроллер полноэкранной рекламы
 */
- (void)presentFromRootViewController:(UIViewController *)rootViewController;

/**
 *  Метод предзагружает рекламу и отображает ее в отдельном полноэкранном view controller'е. На время предзагрузки отображает изображение image на окне window
 *
 *  @param window окно, от которого будет отображаться изображение image и контроллер полноэкранной рекламы
 *  @param image изображение, которое будет отображаться на время предзагрузки рекламы. Обычно это Default.png
 */
- (void)loadAndDisplayRequest:(IVGRequest *)request usingWindow:(UIWindow *)window initialImage:(UIImage *)image;

/**
 *  Метод предзагружает рекламу и отображает ее в отдельном полноэкранном view controller'е. На время предзагрузки отображает изображение image на окне window
 *
 *  @param window окно, от которого будет отображаться изображение image и контроллер полноэкранной рекламы
 *  @param image изображение, которое будет отображаться на время предзагрузки рекламы. Обычно это Default.png
 *  @param shouldShowLoader показывать ли activity indicator
 */
- (void)loadAndDisplayRequest:(IVGRequest *)request usingWindow:(UIWindow *)window initialImage:(UIImage *)image showLoader:(BOOL)shouldShowLoader;


- (void)loadDictionary:(NSDictionary *)dictionary;

@end
