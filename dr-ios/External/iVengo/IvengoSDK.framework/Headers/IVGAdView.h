//
//  IVGAdView.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IVGAdType.h"
#import "IVGAdViewDelegate.h"

@class IVGRequest;

/**
 *  Класс представления баннерного места отображающий баннерную и видеорекламу
 */
@interface IVGAdView : UIView

/**
 *  Обязательное свойство указывающее типоразмер рекламы, которая должна отображаться для этого баннерного места.
 *  Типоразмер обязан быть не полноэкранным, т.е. IVGAdTypeIsInterstitial для него должен возвращать NO
 */
@property(nonatomic, assign) IVGAdType adType;

/**
 *  Опциональное свойство рекламного места, для разделения статистики в личном кабинете.
 */
@property(nonatomic, copy) NSString *adPlace;

/**
 *  Опциональное свойство. Объект который получает все изменения состояния баннерного места описанные в IVGAdViewDelegate
 */
@property(nonatomic, weak) id <IVGAdViewDelegate>delegate;

/**
 *  Конструктор IVGAdView
 *
 *  @param type   типоразмер рекламы отображаемой для этого рекламного места
 *  @param origin координата левого верхнего угла баннера в координатах родительского view объекта (superview)
 *
 *  @return Возвращает новый объект класса
 */
- (id)initWithAdType:(IVGAdType)type origin:(CGPoint)origin;

/**
 *  Конструктор IVGAdView аналогичный -initWithAdType:origin: В качестве координаты левого верхнего угла используется (0, 0)
 *
 *  @param type   типоразмер рекламы отображаемой для этого рекламного места
 *
 *  @return Возвращает новый объект класса
 */
- (id)initWithAdType:(IVGAdType)type;

/**
 *  Метод запускает загрузку рекламы с указанным объектом запроса
 *
 *  @param request объект запроса. В большинстве случаев нужно передавать [IVGRequest defaultRequest]
 */
- (void)loadRequest:(IVGRequest *)request;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (void)play;

@end
