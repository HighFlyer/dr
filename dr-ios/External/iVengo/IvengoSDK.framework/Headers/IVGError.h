//
//  IVGError.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Домен ошибок IvengoSDk
 */
extern NSString *const kIVGErrorDomain;

extern NSInteger const kIVGRequestTimeoutStatusCode;

/**
 *  Коды ошибок возвращаемых сдк
 */
typedef NS_ENUM(NSInteger, IVGErrorCode) {
    /**
     *  Неправильный запрос
     */
    IVGErrorInvalidRequest,
    /**
     *  Запрос рекламы прошёл успешно, но сервер не отдал рекламы
     */
    IVGErrorNoFill,
    /**
     *  Ошибка соединения. Например: отсутствует интернет соединение
     */
    IVGErrorNetworkError,
    /**
     *  Ошибка сервера
     */
    IVGErrorServerError,
    /**
     *  Таймаут загрузки рекламы
     */
    IVGErrorTimeout,
    /**
     *  Указан неверный размер рекламы
     */
    IVGErrorInvalidAdSize
};

@interface IVGError : NSError

@end
