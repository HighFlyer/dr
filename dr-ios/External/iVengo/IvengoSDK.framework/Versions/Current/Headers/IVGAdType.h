//
//  IVGAdType.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

typedef NS_ENUM(NSInteger, IVGAdMediaType) {
    IVGUnknownMedia = 0,
    IVGBannerAd = 1,
    IVGVideoAd = 2
};

/**
 *  Структура описывающая типоразмер баннера.
 *  Всегда используйте одну из имеющихся констант kIVGAdType* и не создавайте структуру вручную.
 *  Для получения информации о размере или типе, а также для стравнения структур используйте соотвествующие функции
 */
typedef struct IVGAdType {
    CGSize size;
    IVGAdMediaType media;
} IVGAdType;

#pragma mark Standard Sizes

/**
 *  Стандартный баннер размера 320x50
 */
extern IVGAdType const kIVGAdTypeBannerStandart;
/**
 *  Баннер-куб размером 320x320
 */
extern IVGAdType const kIVGAdTypeBannerCube;
/**
 *  Полноэкранный баннер
 */
extern IVGAdType const kIVGAdTypeBannerInterstitial;
/**
 *  Видеореклама размером 320x50
 */
extern IVGAdType const kIVGAdTypeVideoSlim;
/**
 *  Видеореклама-куб размером 320x320
 */
extern IVGAdType const kIVGAdTypeVideoCube;
/**
 *  Полноэкранная видеореклама
 */
extern IVGAdType const kIVGAdTypeVideoInterstitial;

/**
 *  Константа обозначающая невалидный типоразмер баннера
 */
extern IVGAdType const kIVGAdTypeInvalid;


#pragma mark Convenience Functions

/**
 *  Возвращает является ли переданный типоразмер рекламы допустимым для запроса
 *
 *  @param adType тестируемый типоразмер рекламы
 *
 *  @return YES если баннер может быть запрошен
 */
BOOL IVGAdTypeIsValid(IVGAdType adType);

/**
 *  Функция сравнения типоразмеров баннера
 *
 *  @param adType1 типоразмер 1
 *  @param adType2 типоразмер 1
 *
 *  @return YES если переданные типоразмеры совпадают
 */
BOOL IVGAdTypeEqualToAdType(IVGAdType adType1, IVGAdType adType2);

/**
 *  Функция возвращает размер рекламной области
 *
 *  @param adType типоразмер рекламы
 *
 *  @return размер рекламы
 */
CGSize CGSizeFromIVGAdType(IVGAdType adType);

/**
 *  Возвращает является ли указанный типоразмер полноэкранным
 *
 *  @param adType тестируемый типоразмер рекламы
 *
 *  @return YES если типоразмер полноэкранный
 */
BOOL IVGAdTypeIsInterstitial(IVGAdType adType);

/**
 *  Возвращает является ли указанный типоразмер баннерной рекламой
 *
 *  @param adType тестируемый типоразмер рекламы
 *
 *  @return YES если типоразмер - баннер
 */
BOOL IVGAdTypeIsBanner(IVGAdType adType);

/**
 *  Возвращает является ли указанный типоразмер видеорекламой
 *
 *  @param adType тестируемый типоразмер рекламы
 *
 *  @return YES если типоразмер - видеореклама
 */
BOOL IVGAdTypeIsVideo(IVGAdType adType);
