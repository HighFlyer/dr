//
//  IVGManager.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Менеджер IvengoSDK. Позволяет настроить параметры для всех рекламных запросов (например app id)
 */
@interface IVGManager : NSObject

/**
 *  Свойство содержит app id вашего приложения полученный при регистрации в Ivengo
 *  Если свойство не указано или содержит не существующий app id - реклама не сможет быть получена
 */
@property (nonatomic, strong, readwrite) NSString *appID;

/**
 *  Синглтон
 *
 *  @return возвращает синглтон объект менеджера
 */
+ (IVGManager *)sharedManager;

@end
