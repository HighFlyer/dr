//
//  IVGBannerRequest.h
//  ivengosdk
//
//  Created by Михаил Любимов on 21.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#import "IVGAdType.h"
#import "IVGAdLoaderDelegate.h"

@class IVGResponse;
@interface IVGAdLoader : NSObject

@property (nonatomic, weak) id <IVGAdLoaderDelegate>delegate;

@property (nonatomic, strong) NSString *appID;
@property (nonatomic, strong, readonly) NSString *apiVersion;
@property (nonatomic, strong, readonly) NSString *platform;
@property (nonatomic, strong, readonly) NSString *sdkVersion;

@property (nonatomic, strong, readonly) NSString *ipv4;
@property (nonatomic, strong, readonly) NSString *ipv6;
@property (nonatomic, assign, readwrite) CGFloat latitude;
@property (nonatomic, assign, readwrite) CGFloat longitude;
@property (nonatomic, assign, readwrite) CGFloat accuracy;

@property (nonatomic, strong, readonly) NSString *deviceID;
@property (nonatomic, assign, readonly) CGSize screenSize;
@property (nonatomic, assign, readonly) CGFloat screenDensity;
@property (nonatomic, strong, readonly) NSString *deviceVendor;
@property (nonatomic, strong, readonly) NSString *deviceModel;
@property (nonatomic, strong, readonly) NSString *carrierName;
@property (nonatomic, strong, readonly) NSString *internetAccessTechnology;
@property (nonatomic, strong, readonly) NSString *osName;
@property (nonatomic, strong, readonly) NSString *osVersion;
@property (nonatomic, strong, readonly) NSString *bundleID;
@property (nonatomic, strong, readwrite) NSString *locale;

@property (nonatomic, assign, readwrite) IVGAdType adType;
@property (nonatomic, strong, readwrite) NSString *adUnitID;
@property (nonatomic, strong, readwrite) NSString *adPlace;

@property (nonatomic, strong, readonly) IVGResponse *response;

+ (instancetype)defaultLoader;

- (void)startRequest;

@end
