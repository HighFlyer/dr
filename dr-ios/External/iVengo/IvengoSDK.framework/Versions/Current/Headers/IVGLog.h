//
//  IVGLog.h
//
//  Created by Mikhail on 06.06.13.
//  Copyright (c) 2013 Sebbia. All rights reserved.
//

#ifndef visdk_IVGLog_h
#define visdk_IVGLog_h

#include <stdio.h>
#include <sys/time.h>

typedef NS_ENUM(NSInteger, IVGLogLevel) {
	IVGLogFatal		= 0,
	IVGLogError		= 1,
	IVGLogWarn		= 2,
	IVGLogDebug		= 3,
	IVGLogVerbose	= 4
};

extern IVGLogLevel ivgloglevel;

#ifdef DEBUG

#define IVGLog(type, fmt, ...)      {\
								struct timeval tim;\
								struct timezone tzp;\
								gettimeofday(&tim, &tzp);\
								time_t rawtime;\
								struct tm * timeinfo;\
								time ( &rawtime );\
								timeinfo = localtime ( &rawtime );\
								NSString* mainString = [NSString stringWithFormat:fmt, ##__VA_ARGS__];\
								NSString* newBody = [mainString stringByReplacingOccurrencesOfString:@"\n" withString:@"\n\t\t"];\
								const char* body = [newBody UTF8String];\
								float seconds = timeinfo->tm_sec + tim.tv_usec / 1000000.0;\
								/*printf("%02d:%02d:%06.3f l:%3d |<%s> %s (%s)\n", timeinfo->tm_hour, timeinfo->tm_min, seconds, __LINE__, [type UTF8String], body, __PRETTY_FUNCTION__);\*/ \
                                printf("%02d:%02d:%06.3f l:%3d |<%s> %s\n", timeinfo->tm_hour, timeinfo->tm_min, seconds, __LINE__, [type UTF8String], body);\
							}

#else

#define IVGLog(type, fmt, ...) {\
								NSString* mainString = [NSString stringWithFormat:fmt, ##__VA_ARGS__];\
								NSString* newBody = [mainString stringByReplacingOccurrencesOfString:@"\n" withString:@"\n\t\t"];\
								NSLog(@"<%@> %@ (%s)", type, newBody, __PRETTY_FUNCTION__);\
}

#endif

#define IVGLogF(fmt, ...)     if (ivgloglevel >= IVGLogFatal) { IVGLog(@"fatal", fmt, ##__VA_ARGS__); }
#define IVGLogE(fmt, ...)     if (ivgloglevel >= IVGLogError) { IVGLog(@"error", fmt, ##__VA_ARGS__); }
#define IVGLogW(fmt, ...)     if (ivgloglevel >= IVGLogWarn) { IVGLog(@"warn ", fmt, ##__VA_ARGS__); }
#define IVGLogD(fmt, ...)     if (ivgloglevel >= IVGLogDebug) { IVGLog(@"debug", fmt, ##__VA_ARGS__); }
#define IVGLogV(fmt, ...)     if (ivgloglevel >= IVGLogVerbose) { IVGLog(@"verbose", fmt, ##__VA_ARGS__); }

#endif
