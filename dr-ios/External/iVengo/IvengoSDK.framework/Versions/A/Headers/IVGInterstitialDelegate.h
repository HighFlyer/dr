//
//  IVGInterstitialDelegate.h
//  ivengosdk
//
//  Created by Михаил Любимов on 29.05.14.
//  Copyright (c) 2014 Sebbia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IVGInterstitial;
@class IVGError;

/**
 *  Протокол объекта-делегата полноэкранной рекламы IVGInterstitial. Получает уведомления об изменении состояния рекламы
 */
@protocol IVGInterstitialDelegate <NSObject>
@optional

/**
 *  Метод вызывается в момент окончания загрузки и кеширования рекламы. Начиная с этого момента реклама может быть отображена
 *
 *  @param interstitial реклама
 */
- (void)interstitialDidReceiveAd:(IVGInterstitial *)interstitial;

/**
 *  Метод вызывается в случае если загрузка или кеширование рекламы завершились ошибкой.
 *
 *  @param interstitial реклама
 *  @param error        объект ошибки
 */
- (void)interstitial:(IVGInterstitial *)interstitial didFailWithError:(IVGError *)error;

/**
 *  Метод вызывается в момент начала отображения рекламы
 *
 *  @param interstitial реклама
 */
- (void)interstitialDidShowAd:(IVGInterstitial *)interstitial;

/**
 *  Метод вызывается, когда пользователь сам закрыл рекламу. Кнопка "Пропустить" в видеоролике или кнопка закрытия в баннере.
 *
 *  @param interstitial реклама
 */
- (void)interstitialDidSkipAd:(IVGInterstitial *)interstitial;

/**
 *  Метод вызывается, когда реклама была закрыта самим сдк. Например закончился видеоролик или истёк таймаут показа баннера.
 *
 *  @param interstitial реклама
 */
- (void)interstitialDidFinishAd:(IVGInterstitial *)interstitial;

/**
 *  Метод вызывается, когда пользователь переходит по рекламной ссылке и запускается другое приложение, например браузер Safari
 *
 *  @param interstitial реклама
 *  @param url          ссылка по которой переходит пользователь
 */
- (void)interstitial:(IVGInterstitial *)interstitial willLeaveApplicationWithURL:(NSURL *)url;

@end
