/*
 * Created by alexey on 6/19/13.
 */


#import "QuestionsOrderManager.h"
#import "FMResultSet.h"

static NSString * const kQuestionsOrder = @"key_question_orders";
static NSString * const kOriginalQuestionsCount = @"key_original_questions_count";
static NSString * const kIncorrectAnswersNumber = @"key_incorrect_answers_number";
static NSString * const kStartTime = @"key_start_time";
static NSString * const kTotalDuration = @"key_total_duration";
static NSString * const kAdsFree = @"key_ads_free";

@interface QuestionsOrderManager ()

+ (void) removeAll:(NSString *)obj inArray:(NSMutableArray *)array;

@end

@implementation QuestionsOrderManager

+ (BOOL) doesQuestionOrdersPresents {
    return [[NSUserDefaults standardUserDefaults] stringArrayForKey:kQuestionsOrder] != nil;
}

+ (NSArray *) questionIdsInOrder {
    return [[NSUserDefaults standardUserDefaults] stringArrayForKey:kQuestionsOrder];
}

+ (NSString *) firstQuestionId {
    NSArray * array = [self questionIdsInOrder];
    return array.count > 0 ? [array objectAtIndex:0] : nil;
}

+ (void) fillByResultSet:(FMResultSet *)resultSet {
    NSMutableArray * ids = [NSMutableArray array];
    while ([resultSet next])
        [ids addObject:[resultSet stringForColumn:@"_id"]];

    [[NSUserDefaults standardUserDefaults] setInteger:ids.count forKey:kOriginalQuestionsCount];
    [[NSUserDefaults standardUserDefaults] setFloat:clock() forKey:kStartTime];
    [QuestionsOrderManager storeIds:ids];
}

+ (void) storeIds:(NSArray *)ids {
    [[NSUserDefaults standardUserDefaults] setObject:ids forKey:kQuestionsOrder];
}

+ (void) flush {
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger) originalQuestionsCount {
    return [[NSUserDefaults standardUserDefaults] integerForKey:kOriginalQuestionsCount];
}

+ (NSInteger) uniqueIdsCount {
    NSSet * set = [NSSet setWithArray:[self questionIdsInOrder]];
    return set.count;
}

+ (void) removeFirstTicket:(NSString *)questionId {
    NSMutableArray * ids = [NSMutableArray arrayWithArray:[self questionIdsInOrder]];
    if (ids.count <= 0)
        return;

    NSString * firstId = [ids objectAtIndex:0];
    if (![firstId isEqualToString:questionId])
        return;

    [ids removeObjectAtIndex:0];
    [self storeIds:ids];
}

+ (void) removeFirstTicketAndShuffle:(NSString *)questionId {
    NSMutableArray * ids = [NSMutableArray arrayWithArray:[self questionIdsInOrder]];
    if (ids.count <= 0)
        return;

    NSString * id = [ids objectAtIndex:0];
    if (![questionId isEqualToString:id])
        return;

    [self removeAll:id inArray:ids];

    NSInteger countMinusOne = ids.count > 0 ? ids.count - 1 : -1;
    [ids insertObject:id atIndex:MIN(15, MAX(0, countMinusOne))];
    if (ids.count > 40)
        [ids insertObject:id atIndex:40];

    [self storeIds:ids];
}

+ (NSInteger) incorrectAnswersNumber {
    return [[NSUserDefaults standardUserDefaults] integerForKey:kIncorrectAnswersNumber];
}

+ (void) increaseIncorrectAnswersNumber {
    [[NSUserDefaults standardUserDefaults] setInteger:[self incorrectAnswersNumber] + 1 forKey:kIncorrectAnswersNumber];
}

+ (void) removeAll:(NSString *)obj inArray:(NSMutableArray *)array {
    NSMutableIndexSet * set = [NSMutableIndexSet indexSet];
    for (NSUInteger i = 0; i < array.count; i++)
        if ([[array objectAtIndex:i] isEqualToString:obj])
            [set addIndex:i];

    [array removeObjectsAtIndexes:set];
}

+ (void) removeAllSettings {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kQuestionsOrder];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kOriginalQuestionsCount];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kIncorrectAnswersNumber];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kStartTime];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTotalDuration];

    [self flush];
}

+ (float) startTime {
    return [[NSUserDefaults standardUserDefaults] floatForKey:kStartTime];
}

+ (float) totalDuration {
    return [[NSUserDefaults standardUserDefaults] floatForKey:kTotalDuration];
}

+ (void) increaseTotalDurationBy:(float)delta {
    delta = ABS(delta);
    [[NSUserDefaults standardUserDefaults] setFloat:[self totalDuration] + delta forKey:kTotalDuration];
}

+ (void) setAdsFree {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kAdsFree];
}

+ (BOOL) isAdsFree {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kAdsFree];
}

@end
