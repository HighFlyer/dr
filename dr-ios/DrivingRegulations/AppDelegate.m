//
//  AppDelegate.m
//  DrivingRegulations
//
//  Created by Alexey Nikitin on 6/18/13.
//  Copyright (c) 2013 Alexey Nikitin. All rights reserved.
//

#import <IvengoSDK/IvengoSDK.h>
#import "AppDelegate.h"
#import "LogUtils.h"
#import "GAI.h"

@implementation AppDelegate

+ (void) initialize {
    [iRate sharedInstance].daysUntilPrompt = 3;
    [iRate sharedInstance].usesUntilPrompt = 5;
    [iRate sharedInstance].appStoreID = 670312489;
}

- (id) init {
    self = [super init];
    if (self) {
        [iRate sharedInstance].delegate = self;
    }

    return self;
}

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [LogUtils startSession];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-46023622-1"];

    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)
        [IVGManager sharedManager].appID = @"ai30zkcu1ny4";

    return YES;
}

- (void) applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void) applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:kEnterBackground
                                                        object:nil
                                                      userInfo:nil];
}

- (void) applicationWillEnterForeground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:kEnterForeground
                                                        object:nil
                                                      userInfo:nil];
}

- (void) applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void) applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark iRateDelegate

- (void) iRateDidPromptForRating {
    [LogUtils logRateDialogShows];
}

- (void) iRateUserDidAttemptToRateApp {
    [LogUtils logRateDialogRateClicked];
}

- (void) iRateUserDidDeclineToRateApp {
    [LogUtils logRateDialogNeverClicked];
}

- (void) iRateUserDidRequestReminderToRateApp {
    [LogUtils logRateDialogLaterClicked];
}

@end
