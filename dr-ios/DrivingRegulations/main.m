//
//  main.m
//  DrivingRegulations
//
//  Created by Alexey Nikitin on 6/18/13.
//  Copyright (c) 2013 Alexey Nikitin. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
