/*
 * Created by alexey on 6/18/13.
 */


#import <Foundation/Foundation.h>

@class FMDatabase;

@interface DBManager : NSObject

+ (DBManager *) instance;

@property (nonatomic, strong) FMDatabase * db;

@end
