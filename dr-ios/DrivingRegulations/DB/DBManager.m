/*
 * Created by alexey on 6/18/13.
 */


#import "DBManager.h"
#import "FMDatabase.h"

@interface DBManager ()

@end

@implementation DBManager

- (id) init {
    self = [super init];
    if (self) {
        NSString * path = [[NSBundle mainBundle] pathForResource:@"dr" ofType:@"db"];
        _db = [FMDatabase databaseWithPath:path];
        [_db open];
    }

    return self;
}


+ (DBManager *) instance {
    static DBManager * _instance = nil;

    @synchronized (self) {
        if (_instance == nil)
            _instance = [[self alloc] init];
    }

    return _instance;
}

@end