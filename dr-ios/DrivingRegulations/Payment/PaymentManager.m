/*
 * Created by alexey.nikitin on 26.02.14.
 */


#import <StoreKit/StoreKit.h>
#import "PaymentManager.h"

static NSString * adFreeFeature = @"ru.highflyer.DrivingRegulations.adfree";

@interface PaymentManager () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (nonatomic, strong) SKProductsRequest * request;

@end

@implementation PaymentManager

- (id) init {
    self = [super init];
    if (self) {
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }

    return self;
}

+ (PaymentManager *) instance {
    static PaymentManager * _instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (void) restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) buyAdsFree {
    _request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:adFreeFeature]];
    _request.delegate = self;
    [_request start];
}

#pragma mark - SKProductsRequestDelegate

- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"Products list found!");
    _request = nil;

    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Product found: %@ %@ %0.2f",
                skProduct.productIdentifier,
                skProduct.localizedTitle,
                skProduct.price.floatValue);

        if ([adFreeFeature isEqualToString:skProduct.productIdentifier]) {
            NSLog(@"Try to buy: %@", skProduct);
            SKPayment * payment = [SKPayment paymentWithProduct:skProduct];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            return;
        }
    }

    [_delegate adsFreeBuyFailed:@"У программиста кривые руки, сообщите ему об этом :)"];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed to load products list: %@", error);
    _request = nil;

    [_delegate adsFreeBuyFailed:error.localizedDescription];
}

#pragma mark SKPaymentTransactionObserver

- (void) paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction * transaction in transactions) {
        if (![adFreeFeature isEqualToString:transaction.payment.productIdentifier])
            continue;

        switch (transaction.transactionState) {
        case SKPaymentTransactionStatePurchased:
            [self completeTransaction:transaction];
            break;
        case SKPaymentTransactionStateFailed:
            [self failedTransaction:transaction];
            break;
        case SKPaymentTransactionStateRestored:
            [self restoreTransaction:transaction];
        default:
            break;
        }
    }
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");

    [_delegate adsFreeBuyed];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");

    [_delegate adsFreeBuyed];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {

    NSLog(@"failedTransaction...");
    [_delegate adsFreeBuyFailed:transaction.error.localizedDescription];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

@end
