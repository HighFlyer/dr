/*
 * Created by alexey.nikitin on 26.02.14.
 */


#import <Foundation/Foundation.h>

@protocol PaymentManagerDelegate

- (void) adsFreeBuyed;
- (void) adsFreeBuyFailed:(NSString *)errorMessage;

@end

@interface PaymentManager : NSObject

@property (nonatomic, weak) id<PaymentManagerDelegate> delegate;

+ (PaymentManager *) instance;
- (void) buyAdsFree;
- (void) restoreCompletedTransactions;

@end