//
//  AppDelegate.h
//  DrivingRegulations
//
//  Created by Alexey Nikitin on 6/18/13.
//  Copyright (c) 2013 Alexey Nikitin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iRate.h"

static NSString * const kEnterBackground = @"enter_background";
static NSString * const kEnterForeground = @"enter_foreground";

@interface AppDelegate : UIResponder <UIApplicationDelegate, iRateDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
