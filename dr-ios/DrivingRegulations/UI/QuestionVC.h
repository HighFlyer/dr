/*
 * Created by alexey on 6/18/13.
 */


#import <Foundation/Foundation.h>
#import <IvengoSDK/IvengoSDK.h>
#import "GADBannerViewDelegate.h"
#import "GAITrackedViewController.h"
#import "PaymentManager.h"
#import "SelectQuestionVC.h"

@interface QuestionVC : GAITrackedViewController
        <UIAlertViewDelegate, GADBannerViewDelegate,
        UIActionSheetDelegate, PaymentManagerDelegate, SelectQuestionDelegate, IVGInterstitialDelegate>

+ (void) configureButtonWithImageName:(UIButton *)button buttonName:(NSString *)buttonName;

@end