/*
 * Created by alexey.nikitin on 15.03.14.
 */


#import <Foundation/Foundation.h>

@interface ImageVC : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIImage * image;

@end