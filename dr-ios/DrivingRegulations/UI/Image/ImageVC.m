/*
 * Created by alexey.nikitin on 15.03.14.
 */


#import "ImageVC.h"

@interface ImageVC ()

@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UIImageView * imageView;

- (void) closeClicked:(id)sender;
- (void) configureScrollView;

@end

@implementation ImageVC

- (void) viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem * button =
            [[UIBarButtonItem alloc]
                    initWithTitle:@"Закрыть"
                            style:UIBarButtonItemStylePlain
                           target:self
                           action:@selector(closeClicked:)];
    button.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = button;
    _imageView.image = _image;
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    [self configureScrollView];
}

- (void) configureScrollView {
    _imageView.frame = CGRectMake(0, 0, _image.size.width, _image.size.height);

    _scrollView.contentSize = _imageView.frame.size;
    _scrollView.decelerationRate = UIScrollViewDecelerationRateFast;

    _scrollView.contentMode = UIViewContentModeScaleAspectFit;
    _scrollView.zoomScale = self.view.frame.size.width / _imageView.frame.size.width;
}

- (void) closeClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}

@end