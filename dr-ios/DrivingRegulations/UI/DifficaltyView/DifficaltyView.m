/*
 * Created by alexey on 10/6/13.
 */


#import "DifficaltyView.h"

@implementation DifficaltyView

- (void) drawRect:(CGRect)rect {
    [super drawRect:rect];

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGFloat colors [] = {
            1 - _ease, _ease, 0, 1.0
    };

    CGContextSaveGState(context);
        CGContextSetFillColor(context, colors);
        CGContextAddEllipseInRect(context, self.bounds);
        CGContextFillPath(context);
    CGContextRestoreGState(context);
}

@end