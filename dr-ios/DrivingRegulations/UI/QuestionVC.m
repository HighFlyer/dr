/*
 * Created by alexey on 6/18/13.
 */


#import "QuestionVC.h"
#import "DBManager.h"
#import "FMDatabase.h"
#import "GADBannerView.h"
#import "QuestionsOrderManager.h"
#import "LogUtils.h"
#import "ProgressView.h"
#import "DifficaltyView.h"
#import "ImageVC.h"
#import "AppDelegate.h"

#define BUTTON_MARGIN ([QuestionVC isPhone] ? 5 : 10)

static NSInteger const kTagRepeat = 0;
static NSInteger const kTagOptions = 1;

static NSInteger const kTagBuy = 0;

typedef enum {
    DisplayModeQuestion,
    DisplayModeError
} DisplayMode;

@interface QuestionVC ()

@property (nonatomic, weak) IBOutlet UILabel * progress;
@property (nonatomic, weak) IBOutlet UILabel * questionName;
@property (nonatomic, weak) IBOutlet UIImageView * questionBackground;
@property (nonatomic, weak) IBOutlet UIImageView * image;
@property (nonatomic, weak) IBOutlet UIButton * imageButton;
@property (nonatomic, weak) IBOutlet UIView * answersBlock;
@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UIButton * googlePlusMenuButton;
@property (nonatomic, weak) IBOutlet ProgressView * progressView;
@property (nonatomic, weak) IBOutlet DifficaltyView * difficaltyView;
@property (nonatomic, weak) IBOutlet UIView * separator;
@property (nonatomic, weak) IBOutlet UILabel * tipLabel;
@property (nonatomic, weak) IBOutlet UIImageView * tipBackground;
@property (nonatomic, weak) IBOutlet UIButton * understandButton;
@property (nonatomic, strong) GADBannerView * bannerView;
@property (nonatomic, strong) UIBarButtonItem * dontKnowButton;
@property (nonatomic, assign) CGFloat lastIvengoBannerShowTime;

@property (nonatomic, strong) NSString * ticketNumber;
@property (nonatomic, strong) NSString * questionNumber;
@property (nonatomic, strong) NSString * questionId;
@property (nonatomic, assign) long questionStartTime;
@property (nonatomic, strong) IVGInterstitial * iVengoInterstitial;
@property (nonatomic, assign) DisplayMode currentMode;

- (IBAction) googlePlusClicked;
- (IBAction) understandClicked;

- (void) configureQuestionViewWithResultSet:(FMResultSet *)resultSet andProgressString:(NSString *)progressString;
- (void) checkForFirstLaunch;
- (void) initUI;
- (void) updateForNextQuestion;
- (void) initAdMob;
- (void) initIvengo;
- (void) showRepeatDialog;

- (NSString *) buildProgressString:(NSInteger)remainingCount;
- (void) configureAnswersWithResultSet:(FMResultSet *)resultSet;
- (void) layoutChildViews;
- (void) answerButtonClicked:(UIButton *)button;
- (void) addDontKnowButton;
- (void) dontKnowClicked:(id)sender;
- (void) startQuestion;
- (void) prepareForScreenshot;
+ (BOOL) isPhone;
- (void) roundImageView;
- (void) processAdsFreeClicked;
- (void) updateUIById:(NSString *)progressString questionId:(NSString *)questionId;
- (void) showIvengoAds;
- (void) clearAdvertisments;
- (UIButton *) createButton;

@end

@implementation QuestionVC

- (void) viewDidLoad {
    [super viewDidLoad];

    [PaymentManager instance].delegate = self;
    if (![QuestionsOrderManager isAdsFree]) {
        [self initAdMob];
        [self initIvengo];
    }

    [self addDontKnowButton];
    [self initUI];

    [self startQuestion];
//    [self prepareForScreenshot];
//    [self showRepeatDialog];

    self.navigationController.navigationBar.translucent = NO;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    else
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForeground) name:kEnterForeground object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackground) name:kEnterBackground object:nil];
}

- (void) viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) enterForeground {
    NSLog(@"enterForeground");
    _questionStartTime = clock();
}

- (void) enterBackground {
    NSLog(@"enterBackground");

    long answeringTime = clock() - _questionStartTime;
    [QuestionsOrderManager increaseTotalDurationBy:(float)answeringTime / CLOCKS_PER_SEC];
    [QuestionsOrderManager flush];
}

- (void) roundImageView {
    /* See http://stackoverflow.com/questions/4847163/round-two-corners-in-uiview */

    // set the radius
    CGFloat radius = 7.0;
    // set the mask frame, and increase the height by the
    // corner radius to hide bottom corners
    CGRect maskFrame = self.image.bounds;
    maskFrame.size.height += radius;
    // create the mask layer
    CALayer * maskLayer = [CALayer layer];
    maskLayer.cornerRadius = radius;
    maskLayer.backgroundColor = [UIColor blackColor].CGColor;
    maskLayer.frame = maskFrame;

    // set the mask
    _image.layer.mask = maskLayer;
}

- (void) viewDidAppear:(BOOL)animated {
    self.screenName = @"QuestionVC";

    [super viewDidAppear:animated];
}

- (void) initUI {
    _questionName.font = [UIFont systemFontOfSize:[QuestionVC isPhone] ? 16 : 20];
    _tipLabel.font = [UIFont systemFontOfSize:[QuestionVC isPhone] ? 16 : 20];

    UIImage * uiImage = [UIImage imageNamed:@"button_single.png"];
    UIImage * capHeight = [uiImage stretchableImageWithLeftCapWidth:8 topCapHeight:7];
    [_tipBackground setImage:capHeight];

    _understandButton.titleLabel.font = [UIFont systemFontOfSize:_questionName.font.pointSize];
    [QuestionVC configureButtonWithImageName:_understandButton buttonName:@"button_single"];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    [self layoutChildViews];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self enterForeground];
}

+ (BOOL) isPhone {
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

- (void) prepareForScreenshot {
    _progress.text = nil;
    _questionName.text = nil;
    _difficaltyView.hidden = YES;
    _separator.hidden = YES;
}

- (void) startQuestion {
    [self checkForFirstLaunch];
    [self updateForNextQuestion];
}

- (void) addDontKnowButton {
    _dontKnowButton =
            [[UIBarButtonItem alloc]
                    initWithTitle:@"Не знаю"
                            style:UIBarButtonItemStylePlain
                           target:self
                           action:@selector(dontKnowClicked:)];
    _dontKnowButton.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = _dontKnowButton;
}

- (void) dontKnowClicked:(id)sender {
    [LogUtils logDontKnowClicked:[QuestionsOrderManager firstQuestionId]];
    [self showTipAlert];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self enterBackground];
}

- (void) updateForNextQuestion {
    [self showIvengoAds];

    NSArray * ids = [QuestionsOrderManager questionIdsInOrder];
    if (ids.count <= 0) {
        [self showRepeatDialog];
        return;
    }

    NSString * progressString = [self buildProgressString:ids.count];
    NSString * questionId = [ids objectAtIndex:0];
    [self updateUIById:progressString questionId:questionId];
}

- (void) updateUIById:(NSString *)progressString questionId:(NSString *)questionId {
    [LogUtils logPageView];

    _questionId = questionId;
    FMResultSet * resultSet = [[DBManager instance].db executeQuery:@"SELECT * FROM QUESTION WHERE _id = ?", questionId];
    [self configureQuestionViewWithResultSet:resultSet andProgressString:progressString];
    [resultSet close];

    resultSet = [[DBManager instance].db executeQuery:@"SELECT * FROM ANSWER WHERE question_id = ? ORDER BY RANDOM()", questionId];
    [self configureAnswersWithResultSet:resultSet];
    [resultSet close];

    [self layoutChildViews];
}

- (void) showRepeatDialog {
    [QuestionsOrderManager increaseTotalDurationBy:(float)(clock() - _questionStartTime) / CLOCKS_PER_SEC];

    if ([QuestionsOrderManager startTime] > 0) {
        long duration = (long)([QuestionsOrderManager totalDuration] * 1000);
        [LogUtils logTotalDuration:duration];
    }

    [LogUtils logRepeatDialog];

    UIAlertView * alertView =
            [[UIAlertView alloc]
                    initWithTitle:@"Поздравления!"
                          message:@"Все билеты пройдены, удачи на экзаменах! Всё начнётся заново."
                         delegate:self cancelButtonTitle:@"Добро"
                otherButtonTitles:@"Оценить в AppStore-е", nil];
    alertView.tag = kTagRepeat;
    [alertView show];
}

- (void) initAdMob {
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];

    // Specify the ad's "unit identifier". This is your AdMob Publisher ID.
    _bannerView.adUnitID = @"ca-app-pub-7050385072281660/8402563631";
    _bannerView.delegate = self;

    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    _bannerView.rootViewController = self;
    [self.view addSubview:_bannerView];

    // Initiate a generic request to load it with an ad.
    GADRequest * request = [GADRequest request];
    request.testDevices = @[@"GAD_SIMULATOR_ID"];
    [_bannerView loadRequest:request];
}

- (void) initIvengo {
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
        _iVengoInterstitial = [[IVGInterstitial alloc] initWithAdType:kIVGAdTypeBannerInterstitial];
        _iVengoInterstitial.delegate = self;
        [_iVengoInterstitial loadRequest:[IVGRequest defaultRequest]];
    }
}

- (void) checkForFirstLaunch {
    if ([QuestionsOrderManager doesQuestionOrdersPresents])
        return;

    FMResultSet * resultSet = [[DBManager instance].db executeQuery:@"SELECT DISTINCT _id FROM QUESTION ORDER BY RANDOM()"];
    [QuestionsOrderManager fillByResultSet:resultSet];
    [resultSet close];

    _questionStartTime = clock();
}

- (NSString *) buildProgressString:(NSInteger)remainingCount {
    NSInteger originalCount = [QuestionsOrderManager originalQuestionsCount];
    CGFloat completeFraction = (CGFloat)(originalCount - [QuestionsOrderManager uniqueIdsCount]) / originalCount;
    CGFloat completePercent = completeFraction * 100.0f;
    _progressView.progress = completeFraction;
    return  [NSString stringWithFormat:@"Прогресс: %0.2f%% (%d)", completePercent, remainingCount];
}

- (void) configureQuestionViewWithResultSet:(FMResultSet *)resultSet andProgressString:(NSString *)progressString {
    if (![resultSet next])
        return;

    CGFloat oldWidth = _questionName.frame.size.width;
    _questionName.text = [resultSet stringForColumn:@"_text"];
    [_questionName sizeToFit];
    CGRect frame = _questionName.frame;
    frame.size.width = oldWidth;
    _questionName.frame = frame;

    _tipLabel.text = [resultSet stringForColumn:@"comment"];

    _ticketNumber = [resultSet stringForColumn:@"ticket_number"];
    _questionNumber = [resultSet stringForColumn:@"question_number"];

    _progress.text = [NSString stringWithFormat:@"Билет %@ Вопрос %@ - %@", _ticketNumber, _questionNumber, progressString];
    NSData * data = [resultSet dataForColumn:@"image_data"];
    UIImage * image = [UIImage imageWithData:data];
    _image.image = image;

    UIImage * uiImage = [UIImage imageNamed:image ? @"button_bottom.png" : @"button_single.png"];
    UIImage * capHeight = [uiImage stretchableImageWithLeftCapWidth:8 topCapHeight:7];
    [_questionBackground setImage:capHeight];

//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:1 - ease green:ease blue:0 alpha:1];
    _difficaltyView.ease = [resultSet doubleForColumn:@"ease"];
    [_difficaltyView setNeedsDisplay];

    _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, 0);
}

- (void) configureAnswersWithResultSet:(FMResultSet *)resultSet {
    for (UIView * view in [_answersBlock subviews])
        [view removeFromSuperview];

    int i = 0;
    UIButton * lastButton;

    while ([resultSet next]) {
        lastButton = [self createButton];

        NSString * text = [resultSet stringForColumn:@"_text"];
        [lastButton setTitle:text forState:UIControlStateNormal];

        NSString * buttonName;
        if (i == 0)
            buttonName = @"button_top";
        else
            buttonName = @"button_center";

        [QuestionVC configureButtonWithImageName:lastButton buttonName:buttonName];

        lastButton.tag = [resultSet intForColumn:@"is_correct"];

        [_answersBlock addSubview:lastButton];
        i++;
    }

    if (i > 0)
        [QuestionVC configureButtonWithImageName:lastButton buttonName:@"button_bottom"];
    else {
        UIButton * button = [self createButton];

        [button setTitle:@"Следующий вопрос" forState:UIControlStateNormal];
        button.tag = -1;

        [QuestionVC configureButtonWithImageName:button buttonName:@"button_single"];

        [_answersBlock addSubview:button];
    }
}

- (UIButton *) createButton {
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];

    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = [UIFont systemFontOfSize:_questionName.font.pointSize];

    float c = 33.0f / 255;
    float cDisabled = 150.0f / 255;
    UIColor * uiColor = [UIColor colorWithRed:c green:c blue:c alpha:1];
    UIColor * uiColorDisabled = [UIColor colorWithRed:cDisabled green:cDisabled blue:cDisabled alpha:1];

    [button setTitleColor:uiColor forState:UIControlStateNormal];
    [button setTitleColor:uiColorDisabled forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(answerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    return button;
}

+ (void) configureButtonWithImageName:(UIButton *)button buttonName:(NSString *)buttonName {
    UIEdgeInsets insets = UIEdgeInsetsMake(8, 8, 8, 8);

    UIImage * image = [UIImage imageNamed:buttonName];
    UIImage * imagePressed = [UIImage imageNamed:[buttonName stringByAppendingString:@"_pressed"]];
    [button setBackgroundImage:[image resizableImageWithCapInsets:insets] forState:UIControlStateNormal];
    [button setBackgroundImage:[imagePressed resizableImageWithCapInsets:insets] forState:UIControlStateHighlighted];
}

- (void) answerButtonClicked:(UIButton *)button {
    BOOL isNoAnswers = button.tag == -1;
    if (isNoAnswers) {
        [QuestionsOrderManager removeFirstTicket:_questionId];
        [self updateForNextQuestion];
    } else {
        BOOL isCorrect = button.tag > 0;

        [LogUtils logQuestion:[QuestionsOrderManager firstQuestionId]
                 ticketNumber:_ticketNumber questionNumber:_questionNumber answered:isCorrect];
        if (isCorrect) {
            [QuestionsOrderManager removeFirstTicket:_questionId];
            [self updateForNextQuestion];
        }
        else {
            [QuestionsOrderManager increaseIncorrectAnswersNumber];
            [self showTipAlert];
        }
    }
}

- (void) showTipAlert {
    _currentMode = DisplayModeError;
    [self layoutChildViews];
}

- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
    case kTagRepeat:
        [QuestionsOrderManager removeAllSettings];
        [self startQuestion];
        switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [LogUtils logThankYouClicked];
            [[UIApplication sharedApplication] openURL:
                    [NSURL URLWithString:@"https://itunes.apple.com/us/app/ekzamen-pdd-2013/id670312489?mt=8"]];
            break;
        }
        break;
    }
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    [self layoutChildViews];
}

- (void) layoutChildViews {
    _dontKnowButton.enabled = _currentMode == DisplayModeQuestion;

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL portrait = orientation == 0 || UIInterfaceOrientationIsPortrait(orientation);
    BOOL isPhone = [QuestionVC isPhone];

    NSLog(@"orientation: %d, %@", orientation, portrait ? @"true" : @"false");
    _bannerView.adSize = portrait ?
            kGADAdSizeSmartBannerPortrait : kGADAdSizeSmartBannerLandscape;

    _bannerView.center = CGPointMake(self.view.frame.size.width / 2,
            self.view.frame.size.height - _bannerView.frame.size.height / 2);

    CGFloat columnWidth = self.view.frame.size.width / (portrait ? 1 : 2) - 2 * BUTTON_MARGIN;
    CGFloat height = _image.image ? columnWidth * _image.image.size.height / _image.image.size.width : 0;
    _image.frame = CGRectMake(BUTTON_MARGIN, _image.frame.origin.y, columnWidth, height);
    _imageButton.frame = _image.frame;

    int mult = isPhone ? 2 : 2;
    CGFloat questionWidth = columnWidth - 2 * mult * BUTTON_MARGIN;
    CGFloat questionHeight =
            [_questionName.text sizeWithFont:_questionName.font constrainedToSize:CGSizeMake(questionWidth, INT_MAX)
                    lineBreakMode:_questionName.lineBreakMode].height;
    _questionName.frame = CGRectMake(BUTTON_MARGIN + mult * BUTTON_MARGIN, _questionName.frame.origin.y,
            columnWidth - 2 * mult * BUTTON_MARGIN, questionHeight);

    int topMargin = BUTTON_MARGIN;
    int bottomMargin = 2 * BUTTON_MARGIN;
    _questionBackground.frame = CGRectMake(BUTTON_MARGIN, _questionBackground.frame.origin.y,
            columnWidth, _questionName.frame.size.height + topMargin + bottomMargin);

    int answerY = 0;
    NSInteger questionTextVertMargin = isPhone ? 10 : 17;
    NSInteger questionTextHorMargin = isPhone ? 6 : 12;
    for (UIButton * button in _answersBlock.subviews) {
        button.enabled = _currentMode == DisplayModeQuestion;

        CGFloat textSize =
                [button.titleLabel.text sizeWithFont:button.titleLabel.font
                 constrainedToSize:CGSizeMake(columnWidth - 2 * (BUTTON_MARGIN + questionTextHorMargin), INT_MAX)
                     lineBreakMode:NSLineBreakByWordWrapping].height;

        button.frame = CGRectMake(0, 0, columnWidth, textSize + 2 * questionTextVertMargin);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, questionTextHorMargin, 0, questionTextHorMargin);

        [QuestionVC setY:answerY forView:button];
        answerY += button.frame.size.height;
        [QuestionVC centerXForView:button];
    }

    _answersBlock.frame = CGRectMake(portrait ? BUTTON_MARGIN : self.view.frame.size.width / 2 + BUTTON_MARGIN,
            _answersBlock.frame.origin.y, columnWidth, answerY);

    CGFloat y = 0;
    CGFloat topY = y;

    [QuestionVC setY:y forView:_image];
    [QuestionVC setY:y forView:_imageButton];
    y += _image.frame.size.height;

    [QuestionVC setY:y forView:_questionBackground];
    [QuestionVC setY:y + BUTTON_MARGIN forView:_questionName];

    y += _questionBackground.frame.size.height;
    y += [QuestionVC isPhone] ? 6 : 10;
    CGFloat answerBottom = y;

    if (portrait) {
        _separator.hidden = NO;
        [QuestionVC setY:y forView:_separator];
        y += [QuestionVC isPhone] ? 7 : 10;
        [QuestionVC centerXForView:_separator];

        [QuestionVC setY:y forView:_answersBlock];
        y += _answersBlock.frame.size.height;
    }
    else {
        _separator.hidden = YES;
        [QuestionVC setY:topY forView:_answersBlock];
    }

    CGFloat answersBottom = _answersBlock.frame.origin.y + _answersBlock.frame.size.height;

    y = MAX(answersBottom, answerBottom);
    y += BUTTON_MARGIN;

    if (_currentMode == DisplayModeError) {
        _tipLabel.hidden = NO;
        _tipBackground.hidden = NO;
        _understandButton.hidden = NO;

        CGFloat tipWidth = self.view.frame.size.width - 2 * BUTTON_MARGIN;
        CGFloat tipHeight =
                [_tipLabel.text sizeWithFont:_tipLabel.font
                           constrainedToSize:CGSizeMake(tipWidth - 2 * (BUTTON_MARGIN + questionTextHorMargin), INT_MAX)
                               lineBreakMode:_tipLabel.lineBreakMode].height;
        _tipLabel.frame = CGRectMake(BUTTON_MARGIN + mult * BUTTON_MARGIN, y + BUTTON_MARGIN,
                tipWidth - 2 * (BUTTON_MARGIN + questionTextHorMargin), tipHeight);

        int topMargin = BUTTON_MARGIN;
        int bottomMargin = 2 * BUTTON_MARGIN;
        _tipBackground.frame = CGRectMake(BUTTON_MARGIN, y,
                tipWidth, _tipLabel.frame.size.height + topMargin + bottomMargin);

        y += tipHeight + topMargin + bottomMargin;
        y += BUTTON_MARGIN;

        CGFloat textSize =
                [_understandButton.titleLabel.text sizeWithFont:_understandButton.titleLabel.font
                                   constrainedToSize:CGSizeMake(tipWidth, INT_MAX)
                                       lineBreakMode:NSLineBreakByWordWrapping].height;

        _understandButton.frame = CGRectMake(0, 0, tipWidth, textSize + 2 * questionTextVertMargin);
        _understandButton.titleEdgeInsets = UIEdgeInsetsMake(0, questionTextHorMargin, 0, questionTextHorMargin);

        [QuestionVC setY:y forView:_understandButton];
        y += _understandButton.frame.size.height;
        [QuestionVC centerXForView:_understandButton];
    } else {
        _tipLabel.hidden = YES;
        _tipBackground.hidden = YES;
        _understandButton.hidden = YES;
    }

    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, y + BUTTON_MARGIN);

    if (_currentMode == DisplayModeError) {
        CGFloat scrollTo = _tipBackground.frame.origin.y;
        CGFloat tipBlockHeight = _understandButton.frame.origin.y + _understandButton.frame.size.height -
                _tipBackground.frame.origin.y;

        CGFloat height = _scrollView.frame.size.height - _scrollView.contentInset.bottom;
        if (tipBlockHeight < height)
            scrollTo -= height - tipBlockHeight;

        if (scrollTo > 0)
            [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, scrollTo) animated:YES];
    }

    [self roundImageView];
}

- (void) showIvengoAds {
    if ([QuestionsOrderManager isAdsFree])
        return;

    if ([_iVengoInterstitial isReady]) {
        [_iVengoInterstitial presentFromRootViewController:self];
    }
    else
        if (!_iVengoInterstitial && !_bannerView) {
            CGFloat deltaTime = CFAbsoluteTimeGetCurrent() - _lastIvengoBannerShowTime;
            if (deltaTime > 2 * 60)
                [self initAdMob];
        }
}

- (IBAction) googlePlusClicked {
    [LogUtils logGooglePlusMenuClicked];

    UIActionSheet * sheet;
    NSString * adsFreeTitle = [QuestionsOrderManager isAdsFree] ? nil : @"Убрать рекламу";

    sheet = [[UIActionSheet alloc]
                initWithTitle:@"Опции"
                     delegate:self
            cancelButtonTitle:@"Отмена"
       destructiveButtonTitle:nil
            otherButtonTitles:@"Выбрать вопрос", adsFreeTitle, nil];

    sheet.tag = kTagOptions;

    [sheet showInView:self.view];
}

- (IBAction) understandClicked {
    _currentMode = DisplayModeQuestion;
    [QuestionsOrderManager removeFirstTicketAndShuffle:_questionId];
    [self updateForNextQuestion];
}

+ (void) setY:(CGFloat)y forView:(UIView *)view {
    view.frame = CGRectMake(view.frame.origin.x, y, view.frame.size.width, view.frame.size.height);
}

+ (void) centerXForView:(UIView *)view {
    view.frame = CGRectMake((view.superview.frame.size.width - view.frame.size.width) / 2, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
}

#pragma mark GADBannerViewDelegate

- (void) adViewDidReceiveAd:(GADBannerView *)view {
    [QuestionVC setY:self.view.frame.size.height - _googlePlusMenuButton.frame.size.height - view.frame.size.height - 4 forView:_googlePlusMenuButton];
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, view.frame.size.height, 0);
    view.hidden = NO;
    [self layoutChildViews];
}

- (void) adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    [QuestionVC setY:self.view.frame.size.height - _googlePlusMenuButton.frame.size.height - 4 forView:_googlePlusMenuButton];
    view.hidden = YES;
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self layoutChildViews];
}

#pragma mark UIActionSheetDelegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
    case kTagOptions:
        switch (buttonIndex) {
        case 0:
            [LogUtils logSelectQuestionClicked];
            [self performSegueWithIdentifier:@"select_question" sender:nil];
            break;
        case 1: {
            [self processAdsFreeClicked];
            break;
        }
        }
        break;
    case kTagBuy: {
        switch (buttonIndex) {
        case 0:
            [LogUtils logAdsFreeClicked];
            if (![QuestionsOrderManager isAdsFree])
                [[PaymentManager instance] buyAdsFree];

            break;
        case 1:
            [LogUtils logAdsFreeRestoreClicked];
            [[PaymentManager instance] restoreCompletedTransactions];

            break;
        }
        }
    }
}

- (void) processAdsFreeClicked {
    [LogUtils logAdsFreeOptionsClicked];
    UIActionSheet * buySheet = [[UIActionSheet alloc]
                    initWithTitle:@"Убрать рекламу"
                         delegate:self
                cancelButtonTitle:@"Отмена"
           destructiveButtonTitle:nil
                otherButtonTitles:@"Впервые", @"Восстановить покупку", nil];
    buySheet.tag = kTagBuy;
    [buySheet showInView:self.view];
}

#pragma mark PaymentManagerDelegate

- (void) adsFreeBuyed {
    [QuestionsOrderManager setAdsFree];

    [self clearAdvertisments];

}

- (void) clearAdvertisments {
    [self adView:_bannerView didFailToReceiveAdWithError:nil];

    [_bannerView removeFromSuperview];
    _bannerView = nil;
    _iVengoInterstitial = nil;
}

- (void) adsFreeBuyFailed:(NSString *)errorMessage {
    NSString * message = @"Не удалось провести транзакцию";
    if (errorMessage.length > 0)
        message = [message stringByAppendingFormat:@": %@", errorMessage];

    UIAlertView * errorAlert = [[UIAlertView alloc]
            initWithTitle:@"Проблема"
                  message:message
                 delegate:nil
        cancelButtonTitle:@"Понять и простить"
        otherButtonTitles:nil];
    [errorAlert show];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];

    NSString * identifier = segue.identifier;
    if ([@"image" isEqualToString:identifier]) {
        UINavigationController * navVC = segue.destinationViewController;
        ImageVC * imageVC = (ImageVC *)navVC.topViewController;
        imageVC.image = _image.image;
    }
    else if ([@"select_question" isEqualToString:identifier]) {
        UINavigationController * navVC = segue.destinationViewController;
        SelectQuestionVC * selectQuestionVC = (SelectQuestionVC *)navVC.topViewController;
        selectQuestionVC.delegate = self;
    }
}

#pragma SelectQuestionDelegate

- (void) selectedTicket:(NSInteger)ticket andQuestion:(NSInteger)question {
    NSLog(@"Select ticket: %d - %d", ticket, question);
    [LogUtils logPageView];
    [LogUtils logQuestionChosen:ticket question:question];

    NSString * ticketString = [NSString stringWithFormat:@"%d", ticket];
    NSString * questionString = [NSString stringWithFormat:@"%d", question];

    FMResultSet * resultSet = [[DBManager instance].db executeQuery:
            @"SELECT _id FROM QUESTION WHERE "
                    "ticket_number = ? AND "
                    "question_number = ?", ticketString, questionString];
    if ([resultSet next]) {
        NSString * questionId = [resultSet stringForColumnIndex:0];

        if (questionId.length > 0) {
            NSLog(@"Question id found: %@", questionId);
            NSArray * ids = [QuestionsOrderManager questionIdsInOrder];
            NSString * progressString = [self buildProgressString:ids.count];
            [self updateUIById:progressString questionId:questionId];
        }
        else
            NSLog(@"question_id is empty");
    }
    else
        NSLog(@"No question found :(");

    [resultSet close];
}

#pragma mark IVGInterstitialDelegate

- (void) interstitialDidReceiveAd:(IVGInterstitial *)interstitial {
    NSLog(@"interstitialDidReceiveAd");
}

- (void) interstitialDidShowAd:(IVGInterstitial *)interstitial {
    NSLog(@"interstitialDidShowAd");
    _lastIvengoBannerShowTime = CFAbsoluteTimeGetCurrent();
    [LogUtils logiVengoShow];
    [self clearAdvertisments];
}

- (void) interstitial:(IVGInterstitial *)interstitial didFailWithError:(IVGError *)error {
    NSLog(@"interstitial:didFailWithError: %@", error);
}

@end
