/*
 * Created by alexey on 7/21/13.
 */


#import "ProgressView.h"

@implementation ProgressView

- (void) drawRect:(CGRect)rect {
    [super drawRect:rect];

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGFloat colors [] = {
            1.0, 0, 0, 1.0,
            1.0 - _progress, _progress, 0, 1.0
    };
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);

    CGContextSaveGState(context);
    CGContextAddRect(context, CGRectMake(0, 0, _progress * self.frame.size.width, 1));
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(self.frame.size.width, 0), 0);
    CGContextRestoreGState(context);
}

@end