/*
 * Created by alexey on 7/21/13.
 */


#import <Foundation/Foundation.h>

@interface ProgressView : UIView

@property (nonatomic, assign) CGFloat progress;

@end