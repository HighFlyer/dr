/*
 * Created by alexey.nikitin on 18.07.14.
 */


#import "SelectQuestionVC.h"
#import "QuestionVC.h"


static const int PADDING = 4;
static const int HEIGHT = 40;

static const int COLUMNS = 5;

@interface SelectQuestionVC ()

@property (nonatomic, assign) NSInteger ticketNumber;

- (void) updateTitle;
- (void) closeClicked:(id)sender;
- (void) buttonClicked:(id)sender;

- (void) updateUI;

- (void) updateButtons;

- (void) createNavigationItem;
@end

@implementation SelectQuestionVC

- (void) viewDidLoad {
    [super viewDidLoad];

    _mode = ModeTicket;
    [self createNavigationItem];
    [self updateUI];
}

- (void) createNavigationItem {
    UIBarButtonItem * button =
            [[UIBarButtonItem alloc]
                    initWithTitle:@"Закрыть"
                            style:UIBarButtonItemStylePlain
                           target:self
                           action:@selector(closeClicked:)];

    button.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = button;
}

- (void) closeClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) buttonClicked:(id)sender {
    int number = ((UIView *)sender).tag;
    NSLog(@"Clicked: %d", number);

    if (_mode == ModeTicket) {
        _ticketNumber = number;
        _mode = ModeQuestion;
        [self updateUI];
    }
    else if (_mode == ModeQuestion) {
        [_delegate selectedTicket:_ticketNumber andQuestion:number];
        [self closeClicked:nil];
    }
}

- (void) updateUI {
    [self updateTitle];
    [self updateButtons];
}

- (void) updateButtons {
    float color = 33.0f / 255;
    UIColor * uiColor = [UIColor colorWithRed:color green:color blue:color alpha:1];

    for (UIView * view in self.view.subviews)
        [view removeFromSuperview];
    int width = (320 - (COLUMNS * PADDING)) / COLUMNS;

    int totalCount = _mode == ModeTicket ? 40 : 20;
    int rows = totalCount / COLUMNS;

    for (int r = 0; r < rows; r++)
        for (int c = 0; c < COLUMNS; c++) {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];

            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            [button setTitle:[NSString stringWithFormat:@"%d", r * 5 + c + 1] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = r * COLUMNS + c + 1;

            button.titleLabel.font = [UIFont systemFontOfSize:14];

            [button setTitleColor:uiColor forState:UIControlStateNormal];

            [QuestionVC configureButtonWithImageName:button buttonName:@"button_single"];

            button.frame = CGRectMake(PADDING / 2 + c * (width + PADDING), PADDING / 2 + r * (HEIGHT + PADDING), width, HEIGHT);

            [self.view addSubview:button];
        }
}

- (void) updateTitle {
    self.title = _mode == ModeTicket ? @"Выберите билет..." : @"...и вопрос";
}

@end