/*
 * Created by alexey.nikitin on 18.07.14.
 */


#import <Foundation/Foundation.h>

typedef enum {
    ModeTicket,
    ModeQuestion
} SelectionMode;

@protocol SelectQuestionDelegate

- (void) selectedTicket:(NSInteger)ticket andQuestion:(NSInteger)question;

@end

@interface SelectQuestionVC : UIViewController

@property (nonatomic, assign) SelectionMode mode;
@property (nonatomic, weak) id<SelectQuestionDelegate> delegate;

@end