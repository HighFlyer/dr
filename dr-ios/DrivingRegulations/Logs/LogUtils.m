/*
 * Created by alexey.nikitin on 21.06.13.
 */


#import "LogUtils.h"
#import "Flurry.h"
#import "GAI.h"

static NSString * const kEventDontKnowClicked = @"DontKnowClicked";
static NSString * const kEventQuestionAnswered = @"QuestionAnswered";
static NSString * const kEventQuestionChosen = @"QuestionChosen";
static NSString * const kEventRepeatShown = @"RepeatShown";
static NSString * const kEventThankYouClicked = @"ThankYouClicked";
static NSString * const kGooglePlusMenuClicked = @"GooglePlusMenuClicked";
static NSString * const kRateDialogShown = @"RateDialogShown";
static NSString * const kRateDialogRateClicked = @"RateDialogRateClicked";
static NSString * const kRateDialogLaterClicked = @"RateDialogLaterClicked";
static NSString * const kRateDialogNeverClicked = @"RateDialogNeverClicked";
static NSString * const kAdsFreeClicked = @"AdsFreeClicked";
static NSString * const kAdsFreeRestoreClicked = @"AdsFreeRestoreClicked";
static NSString * const kAdsFreeOptionsClicked = @"AdsFreeOptionsClicked";
static NSString * const kTotalDuration = @"TotalDuration";
static NSString * const kChooseQuestionClicked = @"ChooseQuestionClicked";
static NSString * const kIvengoShow = @"iVengoShow";

static NSString * const kKeyQuestionId = @"QuestionId";
static NSString * const kKeyQuestionResult = @"QuestionResult";
static NSString * const kKeyIsRight = @"IsRight";
static NSString * const kKeyDuration = @"Duration";

@interface LogUtils ()

+ (void) logEvent:(NSString *)eventName;
+ (void) logEvent:(NSString *)eventName withParams:(NSDictionary *)params;

@end

@implementation LogUtils

+ (void) startSession {
#ifndef DEBUG
    [Flurry startSession:@"D48NTC6K79G32W86NFBP"];
    [Flurry setCrashReportingEnabled:YES];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-46023622-1"];
#else
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
#endif
    [GAI sharedInstance].trackUncaughtExceptions = YES;
}

+ (void) logQuestion:(NSString *)questionId
        ticketNumber:(NSString *)ticketNumber
      questionNumber:(NSString *)questionNumber
            answered:(BOOL)right {
    NSString * eventName = [kEventQuestionAnswered stringByAppendingFormat:@"/%@", ticketNumber];
    NSString * questionResult = [NSString stringWithFormat:@"%@/%@", questionNumber, right ? @"true" : @"false"];
    [self logEvent:eventName withParams:
            [NSDictionary dictionaryWithObjectsAndKeys:questionResult, kKeyQuestionResult, nil]];

    NSString * eventResult = [NSString stringWithFormat:@"%@/%@", questionId, right ? @"true" : @"false"];
    [self logEvent:kEventQuestionAnswered withParams:
            [NSDictionary dictionaryWithObjectsAndKeys:questionId, kKeyQuestionId, right ? @"true" : @"false", kKeyIsRight,
                eventResult, kKeyQuestionResult, nil]];
}

+ (void) logQuestionChosen:(NSInteger)ticket question:(NSInteger)question {
    NSString * eventName = [kEventQuestionChosen stringByAppendingFormat:@"/%d", ticket];
    NSString * questionResult = [NSString stringWithFormat:@"%d", question];
    [self logEvent:eventName withParams:
            [NSDictionary dictionaryWithObjectsAndKeys:questionResult, kKeyQuestionResult, nil]];

    [self logEvent:kEventQuestionChosen];
}

+ (void) logDontKnowClicked:(NSString *)questionId {
    [self logEvent:kEventDontKnowClicked withParams:[NSDictionary dictionaryWithObject:questionId forKey:kKeyQuestionId]];
}

+ (void) logRepeatDialog {
    [self logEvent:kEventRepeatShown];
}

+ (void) logThankYouClicked {
    [self logEvent:kEventThankYouClicked];
}

+ (void) logPageView {
    [Flurry logPageView];
}

+ (void) logGooglePlusMenuClicked {
    [self logEvent:kGooglePlusMenuClicked];
}

+ (void) logRateDialogShows {
    [self logEvent:kRateDialogShown];
}

+ (void) logRateDialogRateClicked {
    [self logEvent:kRateDialogRateClicked];
}

+ (void) logRateDialogLaterClicked {
    [self logEvent:kRateDialogLaterClicked];
}

+ (void) logRateDialogNeverClicked {
    [self logEvent:kRateDialogNeverClicked];
}

+ (void) logAdsFreeClicked {
    [self logEvent:kAdsFreeClicked];
}

+ (void) logAdsFreeRestoreClicked {
    [self logEvent:kAdsFreeRestoreClicked];
}

+ (void) logAdsFreeOptionsClicked {
    [self logEvent:kAdsFreeOptionsClicked];
}

+ (void) logTotalDuration:(long)duration {
    [self logEvent:kTotalDuration withParams:@{kKeyDuration: @(duration)}];
}

+ (void) logSelectQuestionClicked {
    [self logEvent:kChooseQuestionClicked];
}

+ (void) logiVengoShow {
    [self logEvent:kIvengoShow];
}

+ (void) logEvent:(NSString *)eventName {
    [self logEvent:eventName withParams:nil];
}

+ (void) logEvent:(NSString *)eventName withParams:(NSDictionary *)params {
    NSLog(@"%@ - %@", eventName, params);
    [Flurry logEvent:eventName withParameters:params];
}

@end
