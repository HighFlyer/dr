/*
 * Created by alexey.nikitin on 21.06.13.
 */


#import <Foundation/Foundation.h>

@interface LogUtils : NSObject

+ (void) startSession;
+ (void) logQuestion:(NSString *)questionId
        ticketNumber:(NSString *)ticketNumber
      questionNumber:(NSString *)questionNumber
            answered:(BOOL)right;
+ (void) logQuestionChosen:(NSInteger)ticket question:(NSInteger)question;
+ (void) logDontKnowClicked:(NSString *)questionId;
+ (void) logRepeatDialog;
+ (void) logThankYouClicked;
+ (void) logPageView;
+ (void) logGooglePlusMenuClicked;
+ (void) logRateDialogShows;
+ (void) logRateDialogRateClicked;
+ (void) logRateDialogLaterClicked;
+ (void) logRateDialogNeverClicked;
+ (void) logAdsFreeClicked;
+ (void) logAdsFreeRestoreClicked;
+ (void) logAdsFreeOptionsClicked;
+ (void) logTotalDuration:(long)duration;
+ (void) logSelectQuestionClicked;
+ (void) logiVengoShow;

@end
