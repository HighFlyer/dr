/*
 * Created by alexey on 6/19/13.
 */


#import <Foundation/Foundation.h>

@class FMResultSet;

@interface QuestionsOrderManager : NSObject

+ (BOOL) doesQuestionOrdersPresents;
+ (NSArray *) questionIdsInOrder;
+ (NSString *) firstQuestionId;
+ (void) storeIds:(NSArray *)ids;
+ (void) flush;
+ (NSInteger) originalQuestionsCount;
+ (void) fillByResultSet:(FMResultSet *)resultSet;
+ (NSInteger) uniqueIdsCount;
+ (void) removeFirstTicket:(NSString *)questionId;
+ (float) startTime;
+ (void) increaseTotalDurationBy:(float)delta;
+ (float) totalDuration;

+ (NSInteger) incorrectAnswersNumber;
+ (void) increaseIncorrectAnswersNumber;
+ (void) removeFirstTicketAndShuffle:(NSString *)questionId;

+ (void) removeAllSettings;

+ (BOOL) isAdsFree;
+ (void) setAdsFree;

@end
